/*
Navicat MySQL Data Transfer

Source Server         : wf
Source Server Version : 50553
Source Host           : 192.168.1.14:3306
Source Database       : shuimin-group

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2017-09-29 11:26:15
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `r_place_group_type`
-- ----------------------------
DROP TABLE IF EXISTS `r_place_group_type`;
CREATE TABLE `r_place_group_type` (
  `id` varchar(256) DEFAULT NULL,
  `place_id` varchar(256) DEFAULT NULL,
  `group_type_id` varchar(256) DEFAULT NULL,
  `group_type` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of r_place_group_type
-- ----------------------------
INSERT INTO `r_place_group_type` VALUES ('1', '1', '3', '纸牌');
INSERT INTO `r_place_group_type` VALUES ('2', '1', '2', '王者荣耀');
INSERT INTO `r_place_group_type` VALUES ('3', '3', '1', 'dota2');
INSERT INTO `r_place_group_type` VALUES ('4', '2', '2', '王者荣耀');

-- ----------------------------
-- Table structure for `t_follow_group_type`
-- ----------------------------
DROP TABLE IF EXISTS `t_follow_group_type`;
CREATE TABLE `t_follow_group_type` (
  `id` varchar(256) DEFAULT NULL,
  `user_id` varchar(256) DEFAULT NULL,
  `group_type_id` varchar(256) DEFAULT NULL,
  `group_type` varchar(256) DEFAULT NULL,
  `is_delete` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_follow_group_type
-- ----------------------------
INSERT INTO `t_follow_group_type` VALUES ('b204e6fc87e64130913ca071f0831ba9', '1', '1', 'dota2', 'y');

-- ----------------------------
-- Table structure for `t_follow_place`
-- ----------------------------
DROP TABLE IF EXISTS `t_follow_place`;
CREATE TABLE `t_follow_place` (
  `id` varchar(256) DEFAULT NULL,
  `user_id` varchar(256) DEFAULT NULL,
  `place_id` varchar(256) DEFAULT NULL,
  `is_delete` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_follow_place
-- ----------------------------
INSERT INTO `t_follow_place` VALUES ('26dfa44fdabb4bb78e56cf1de3e9eed1', '1', '3', 'y');

-- ----------------------------
-- Table structure for `t_follow_user`
-- ----------------------------
DROP TABLE IF EXISTS `t_follow_user`;
CREATE TABLE `t_follow_user` (
  `id` varchar(256) DEFAULT NULL,
  `user_id` varchar(256) DEFAULT NULL,
  `follow_user_id` varchar(256) DEFAULT NULL,
  `is_delete` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_follow_user
-- ----------------------------
INSERT INTO `t_follow_user` VALUES ('1', '1', '2', 'y');
INSERT INTO `t_follow_user` VALUES ('2', '2', '1', 'n');

-- ----------------------------
-- Table structure for `t_group`
-- ----------------------------
DROP TABLE IF EXISTS `t_group`;
CREATE TABLE `t_group` (
  `id` varchar(256) NOT NULL,
  `group_name` varchar(256) DEFAULT NULL,
  `group_type_id` varchar(256) DEFAULT NULL,
  `group_type` varchar(256) DEFAULT NULL,
  `place_id` varchar(256) DEFAULT NULL,
  `create_time` varchar(256) DEFAULT NULL,
  `book_time` varchar(256) DEFAULT NULL,
  `deadline_time` varchar(256) DEFAULT NULL,
  `group_status` varchar(256) DEFAULT NULL,
  `max_num` varchar(8) DEFAULT NULL,
  `min_num` varchar(8) DEFAULT NULL,
  `duration` varchar(256) DEFAULT NULL,
  `user_id` varchar(256) DEFAULT NULL,
  `remark` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_group
-- ----------------------------
INSERT INTO `t_group` VALUES ('d24bdc94011b4170a96bf1bb7bdc070d', '测试局', '1', 'dota2', '1', '1506064695792', '1506470400000', '1506470800000', 'finish', '20', '12', '200', '08d01cabf0894f679201450c988ca150', null);
INSERT INTO `t_group` VALUES ('f67f516130954116983db1b9d1ade286', 'dsfsdf', '3', '纸牌', '1', '1506064883310', '1511280000000', '1506470900000', 'cancel', null, null, '21', '08d01cabf0894f679201450c988ca150', null);
INSERT INTO `t_group` VALUES ('38e8f7a7ad504f88a65e0bc9fc81b231', '今天', '3', '纸牌', '3', '1506074483556', '1511280000000', '1506470400000', 'waiting', '222', '2', '12', '1d47042e52ca48d5bd44ed3491ca4317', null);
INSERT INTO `t_group` VALUES ('7103be01be1647118b5f35c2bb432932', 'rua人黑', '2', '王者荣耀', '1', '1506605241328', '1506063436067', '1506105241328', 'finish', '15', '10', '60', '08d01cabf0894f679201450c988ca150', null);

-- ----------------------------
-- Table structure for `t_group_member`
-- ----------------------------
DROP TABLE IF EXISTS `t_group_member`;
CREATE TABLE `t_group_member` (
  `id` varchar(256) DEFAULT NULL,
  `group_id` varchar(256) DEFAULT NULL,
  `user_id` varchar(256) DEFAULT NULL,
  `is_creator` varchar(8) DEFAULT NULL,
  `op_status` varchar(8) DEFAULT NULL,
  `op_time` varchar(256) DEFAULT NULL,
  `remark` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_group_member
-- ----------------------------
INSERT INTO `t_group_member` VALUES ('2a074e2bb63048cfaa67b2b24cf63678', 'd24bdc94011b4170a96bf1bb7bdc070d', '08d01cabf0894f679201450c988ca150', 'y', '1', '1506605241329', null);
INSERT INTO `t_group_member` VALUES ('afce97ab8dab492d832bb9ece0bcacfa', 'f67f516130954116983db1b9d1ade286', '08d01cabf0894f679201450c988ca150', 'y', '-1', '1506605241329', null);
INSERT INTO `t_group_member` VALUES ('2bd2841686cb4da4a39d8343f298acd3', '38e8f7a7ad504f88a65e0bc9fc81b231', '1d47042e52ca48d5bd44ed3491ca4317', 'y', '1', '1506605241329', null);
INSERT INTO `t_group_member` VALUES ('7c294412d1e84a0488f6b7e647427ccf', '38e8f7a7ad504f88a65e0bc9fc81b231', '49fadb1076ad413188b5a950ccdb90ed', 'n', '-1', '1506605241329', null);
INSERT INTO `t_group_member` VALUES ('ed7bede4db3d4a1882f0d8dfbc5cda5f', '7103be01be1647118b5f35c2bb432932', '08d01cabf0894f679201450c988ca150', 'y', '1', '1506605241329', null);
INSERT INTO `t_group_member` VALUES ('a3e91147529047a6846c047bf9462aa5', '38e8f7a7ad504f88a65e0bc9fc81b231', '08d01cabf0894f679201450c988ca150', 'n', '1', '1506605847801', null);
INSERT INTO `t_group_member` VALUES ('2458962388f7445a8d010a9aa66dc3af', '38e8f7a7ad504f88a65e0bc9fc81b231', '08d01cabf0894f679201450c988ca150', 'n', '-1', '1506606062196', null);

-- ----------------------------
-- Table structure for `t_group_type`
-- ----------------------------
DROP TABLE IF EXISTS `t_group_type`;
CREATE TABLE `t_group_type` (
  `id` varchar(256) NOT NULL,
  `group_type` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_group_type
-- ----------------------------
INSERT INTO `t_group_type` VALUES ('1', 'dota2');
INSERT INTO `t_group_type` VALUES ('2', '王者荣耀');
INSERT INTO `t_group_type` VALUES ('3', '纸牌');

-- ----------------------------
-- Table structure for `t_place`
-- ----------------------------
DROP TABLE IF EXISTS `t_place`;
CREATE TABLE `t_place` (
  `id` varchar(256) DEFAULT NULL,
  `place_name` varchar(256) DEFAULT NULL,
  `place_address` varchar(256) DEFAULT NULL,
  `place_type_id` varchar(256) DEFAULT NULL,
  `place_type` varchar(256) DEFAULT NULL,
  `place_phone` varchar(256) DEFAULT NULL,
  `gps_x` varchar(256) DEFAULT NULL,
  `gps_y` varchar(256) DEFAULT NULL,
  `creator_id` varchar(256) DEFAULT NULL,
  `create_time` varchar(256) DEFAULT NULL,
  `is_delete` varchar(8) DEFAULT NULL,
  `creator_name` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_place
-- ----------------------------
INSERT INTO `t_place` VALUES ('1', '外婆家', '苏州职业技术学院', '2', '餐厅', '15509091212', '31.262390', '120.756401', '0', '1506063436067', 'n', 'admin');
INSERT INTO `t_place` VALUES ('2', '辛香汇', '苏州职业技术学院A3', '1', '餐厅', '15509091213', '31.260910', '120.754130', '0', '1506063436067', 'n', 'admin');
INSERT INTO `t_place` VALUES ('3', '腾龙', '苏州职业技术学院A', '3', '网吧', '15509091215', '31.259870', '120.753170', '0', '1506063436067', 'n', 'admin');

-- ----------------------------
-- Table structure for `t_place_type`
-- ----------------------------
DROP TABLE IF EXISTS `t_place_type`;
CREATE TABLE `t_place_type` (
  `id` varchar(256) DEFAULT NULL,
  `place_type` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_place_type
-- ----------------------------
INSERT INTO `t_place_type` VALUES ('1', 'KTV');
INSERT INTO `t_place_type` VALUES ('2', '餐厅');
INSERT INTO `t_place_type` VALUES ('3', '网吧');
INSERT INTO `t_place_type` VALUES ('4', '桌游');

-- ----------------------------
-- Table structure for `t_user`
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` varchar(256) NOT NULL,
  `user_phone` varchar(32) DEFAULT NULL,
  `open_id` varchar(256) NOT NULL,
  `real_name` varchar(256) DEFAULT NULL,
  `nick_name` varchar(256) DEFAULT NULL,
  `gender` varchar(8) DEFAULT NULL,
  `avatarUrl` varchar(256) DEFAULT NULL,
  `language` varchar(64) DEFAULT NULL,
  `weixin_no` varchar(64) DEFAULT NULL,
  `country` varchar(64) DEFAULT NULL,
  `province` varchar(64) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', '15501697292', 'wonderfeng20', 'wangfeng', 'wonderfeng', '0', null, null, 'wonderfeng', null, null, null);
INSERT INTO `t_user` VALUES ('2', '15501697293', 'skygu', 'gufei', 'skygu', '0', null, null, 'skygu', null, null, null);
INSERT INTO `t_user` VALUES ('3', '15501697294', 'kun', 'yuxinchi', 'kun', '0', null, null, 'kun', null, null, null);
INSERT INTO `t_user` VALUES ('68d485a6e0dc4b5a999055d0d68bc132', null, '1', null, null, null, null, null, null, null, null, null);
INSERT INTO `t_user` VALUES ('a2d6384767194b3f8176b4222b3e165a', null, '1', null, null, null, null, null, null, null, null, null);
INSERT INTO `t_user` VALUES ('9f5dd0c4db2f42008a8a16d94beac870', null, '1', null, null, null, null, null, null, null, null, null);
INSERT INTO `t_user` VALUES ('217b64fd4fd446ccab03ac370db432f0', null, '1', null, null, null, null, null, null, null, null, null);
INSERT INTO `t_user` VALUES ('3691491501e34dd4b4085d238a2aafc2', null, '1', null, null, null, null, null, null, null, null, null);
INSERT INTO `t_user` VALUES ('49fadb1076ad413188b5a950ccdb90ed', null, 'oLcQQ0ZHrv06iSsIIlK3Gsrh48Oo', null, 'wonderfeng', '1', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIy2cibZOxjMMtFoVkQRKHVTq7qARTfcHMWUyqxpRibwwhg7fZibLGL2vxmlmJCsL2ejTFicz9EaffXSA/0', null, null, 'China', 'Jiangsu', 'Suzhou');
INSERT INTO `t_user` VALUES ('08d01cabf0894f679201450c988ca150', null, 'ooO8f0cNgMYbchDwavRXeCkOmLEI', null, '顾飞', '1', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIT1K5HVp6e11JLic2n37c7zCmahl5Tk48iccSBYwQiaTxZHicGwYdmLicgaG69bfTMbvnYF2z9zb0GoaA/0', null, null, 'China', 'Jiangsu', 'Suzhou');
INSERT INTO `t_user` VALUES ('1d47042e52ca48d5bd44ed3491ca4317', null, 'ooO8f0fN5fB-x_BijPQ1Ffgf5mNQ', null, '坤桑', '1', 'https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83epVibo86CUJNY7nxBMZicY34Mg2yW4VN47A6vnfiaWA3gicUABhFJRc0AEJ1QIrntxLSCYoAOHQYxvSZw/0', null, null, 'DZ', '', '');
INSERT INTO `t_user` VALUES ('85721cc9399142ae8e29efc59f65bdf1', null, 'ooO8f0S40zvQWlEWe-qTilDpyyDY', null, 'Stefano', '1', 'https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoNGu6HlxibSnbORCQBj2qarED8XwcMZsxaz4ynicX2S6MxxxVKWCK0Mr87uBxzHAT9QZqv3yfqEtMw/0', null, null, 'China', 'Jiangsu', 'Suzhou');
INSERT INTO `t_user` VALUES ('491df8eaab79453790594e481411ae07', null, 'ooO8f0VrvMY1lR2wCnWeM5dQ45OE', null, 'wonderfeng', '1', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKoaj91G6VHewnbm7YPXwzSBySrZhpP1ibBSo7O31Nu9uMYDKDldY6cexKZiascGB0zPBP6MQribvV4g/0', null, null, 'China', 'Jiangsu', 'Suzhou');
INSERT INTO `t_user` VALUES ('880dd08c4f1340e795fd43840e54c89b', null, 'ooO8f0Ti2PFIlGm3eeQNnRdxyZl0', null, 'W', '1', 'https://wx.qlogo.cn/mmopen/vi_32/ruvVicDkqK0OE4TUS8uo7jJfux54HNqPcquUpRbNaeH50UMWhojgUc4iaNz79jIBibsibvlLTfsRb26DmN795CKT8A/0', null, null, 'China', 'Jiangsu', 'Xuzhou');
INSERT INTO `t_user` VALUES ('988e421d53f34ac2952ca3a4054d470f', null, 'ooO8f0VsVPaoyG4nqml_WKB6Wy1s', null, 'Kevin', '1', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJcDG5gjWmn8jG9GxKOHgcvWR0j3uy4BrCge0DjJt50VguGJCXPyXaIaFcfY2pd58ddCYehxc4AdQ/0', null, null, 'China', 'Jiangsu', 'Taizhou');

-- ----------------------------
-- View structure for `v_group`
-- ----------------------------
DROP VIEW IF EXISTS `v_group`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_group` AS select `t_group`.`id` AS `id`,`t_group`.`group_name` AS `group_name`,`t_group`.`group_type_id` AS `group_type_id`,`t_group`.`group_type` AS `group_type`,`t_group`.`place_id` AS `place_id`,`t_group`.`create_time` AS `create_time`,`t_group`.`book_time` AS `book_time`,`t_group`.`deadline_time` AS `deadline_time`,`t_group`.`group_status` AS `group_status`,`t_group`.`max_num` AS `max_num`,`t_group`.`min_num` AS `min_num`,`t_group`.`duration` AS `duration`,`t_group`.`user_id` AS `user_id`,`t_group`.`remark` AS `remark`,`t_user`.`user_phone` AS `user_phone`,`t_user`.`open_id` AS `open_id`,`t_user`.`real_name` AS `real_name`,`t_user`.`nick_name` AS `nick_name`,`t_user`.`gender` AS `gender`,`t_user`.`avatarUrl` AS `avatarUrl`,`t_user`.`language` AS `language`,`t_user`.`weixin_no` AS `weixin_no`,`t_user`.`country` AS `country`,`t_user`.`province` AS `province`,`t_user`.`city` AS `city`,`t_place`.`place_name` AS `place_name`,`t_place`.`place_address` AS `place_address`,`t_place`.`place_type_id` AS `place_type_id`,`t_place`.`place_type` AS `place_type`,`t_place`.`place_phone` AS `place_phone`,`t_place`.`gps_x` AS `gps_x`,`t_place`.`gps_y` AS `gps_y`,`t_place`.`creator_id` AS `place_creator_id`,`t_place`.`create_time` AS `place_create_time`,`t_place`.`is_delete` AS `is_delete`,`t_place`.`creator_name` AS `place_creator_name` from ((`t_group` join `t_user` on((`t_group`.`user_id` = `t_user`.`id`))) join `t_place` on((`t_group`.`place_id` = `t_place`.`id`))) ;

-- ----------------------------
-- View structure for `v_group_member`
-- ----------------------------
DROP VIEW IF EXISTS `v_group_member`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_group_member` AS select `t_group_member`.`id` AS `id`,`t_group_member`.`group_id` AS `group_id`,`t_group_member`.`user_id` AS `user_id`,`t_group_member`.`is_creator` AS `is_creator`,`t_group_member`.`op_status` AS `op_status`,`t_group_member`.`op_time` AS `op_time`,`t_group_member`.`remark` AS `remark`,`t_user`.`user_phone` AS `user_phone`,`t_user`.`open_id` AS `open_id`,`t_user`.`real_name` AS `real_name`,`t_user`.`nick_name` AS `nick_name`,`t_user`.`gender` AS `gender`,`t_user`.`avatarUrl` AS `avatarUrl`,`t_user`.`language` AS `language`,`t_user`.`weixin_no` AS `weixin_no`,`t_user`.`country` AS `country`,`t_user`.`province` AS `province`,`t_user`.`city` AS `city` from (`t_group_member` join `t_user` on((`t_group_member`.`user_id` = `t_user`.`id`))) ;

-- ----------------------------
-- View structure for `v_pre_group`
-- ----------------------------
DROP VIEW IF EXISTS `v_pre_group`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_pre_group` AS select `t_group`.`id` AS `id`,`t_group`.`deadline_time` AS `deadline_time`,`t_group`.`group_type` AS `group_type`,`t_group`.`duration` AS `duration`,`t_group`.`group_status` AS `group_status`,`t_group`.`book_time` AS `book_time` from `t_group` ;
