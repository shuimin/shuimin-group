/*
Navicat MySQL Data Transfer

Source Server         : myali
Source Server Version : 50715
Source Host           : rm-uf6977yy7n0g05kk3o.mysql.rds.aliyuncs.com:3306
Source Database       : form-group

Target Server Type    : MYSQL
Target Server Version : 50715
File Encoding         : 65001

Date: 2017-11-02 10:24:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `r_place_group_type`
-- ----------------------------
DROP TABLE IF EXISTS `r_place_group_type`;
CREATE TABLE `r_place_group_type` (
  `id` varchar(256) DEFAULT NULL,
  `place_id` varchar(256) DEFAULT NULL,
  `group_type_id` varchar(256) DEFAULT NULL,
  `group_type` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of r_place_group_type
-- ----------------------------

-- ----------------------------
-- Table structure for `t_follow_group_type`
-- ----------------------------
DROP TABLE IF EXISTS `t_follow_group_type`;
CREATE TABLE `t_follow_group_type` (
  `id` varchar(256) DEFAULT NULL,
  `user_id` varchar(256) DEFAULT NULL,
  `group_type_id` varchar(256) DEFAULT NULL,
  `group_type` varchar(256) DEFAULT NULL,
  `is_delete` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_follow_group_type
-- ----------------------------

-- ----------------------------
-- Table structure for `t_follow_place`
-- ----------------------------
DROP TABLE IF EXISTS `t_follow_place`;
CREATE TABLE `t_follow_place` (
  `id` varchar(256) DEFAULT NULL,
  `user_id` varchar(256) DEFAULT NULL,
  `place_id` varchar(256) DEFAULT NULL,
  `is_delete` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_follow_place
-- ----------------------------

-- ----------------------------
-- Table structure for `t_follow_user`
-- ----------------------------
DROP TABLE IF EXISTS `t_follow_user`;
CREATE TABLE `t_follow_user` (
  `id` varchar(256) DEFAULT NULL,
  `user_id` varchar(256) DEFAULT NULL,
  `follow_user_id` varchar(256) DEFAULT NULL,
  `is_delete` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_follow_user
-- ----------------------------

-- ----------------------------
-- Table structure for `t_group`
-- ----------------------------
DROP TABLE IF EXISTS `t_group`;
CREATE TABLE `t_group` (
  `id` varchar(256) NOT NULL,
  `group_name` varchar(256) DEFAULT NULL,
  `group_type_id` varchar(256) DEFAULT NULL,
  `group_type` varchar(256) DEFAULT NULL,
  `place_id` varchar(256) DEFAULT NULL,
  `create_time` varchar(256) DEFAULT NULL,
  `book_time` varchar(256) DEFAULT NULL,
  `deadline_time` varchar(256) DEFAULT NULL,
  `group_status` varchar(256) DEFAULT NULL,
  `max_num` varchar(8) DEFAULT NULL,
  `min_num` varchar(8) DEFAULT NULL,
  `duration` varchar(256) DEFAULT NULL,
  `user_id` varchar(256) DEFAULT NULL,
  `remark` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_group
-- ----------------------------
INSERT INTO `t_group` VALUES ('8843639c0da940e7a2add34b58c8bf48', '组队打牌', '2', '王者荣耀', '2', '1509430044742', '1509735600000', '1509580800000', 'create', '20', '4', '240', '1846a6f0f12c4ebdbbf4cc951ae64c11', null);

-- ----------------------------
-- Table structure for `t_group_member`
-- ----------------------------
DROP TABLE IF EXISTS `t_group_member`;
CREATE TABLE `t_group_member` (
  `id` varchar(256) DEFAULT NULL,
  `group_id` varchar(256) DEFAULT NULL,
  `user_id` varchar(256) DEFAULT NULL,
  `is_creator` varchar(8) DEFAULT NULL,
  `op_status` varchar(8) DEFAULT NULL,
  `op_time` varchar(256) DEFAULT NULL,
  `remark` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_group_member
-- ----------------------------
INSERT INTO `t_group_member` VALUES ('49c764c7b0a04580a73982cc69abacb3', '8843639c0da940e7a2add34b58c8bf48', '1846a6f0f12c4ebdbbf4cc951ae64c11', 'y', '1', '1509430044742', null);
INSERT INTO `t_group_member` VALUES ('45d4f8e5776e4accb95fc2a62b3bea5d', '5137c4a4de624b70acb9100a67f42488', '1846a6f0f12c4ebdbbf4cc951ae64c11', 'y', '1', '1509430276467', null);
INSERT INTO `t_group_member` VALUES ('da65db44b4a245348e07c2fc1e8ac73f', '8843639c0da940e7a2add34b58c8bf48', '2dcb7aeb9fd247b7b581e668efea6b28', 'n', '1', '1509430519452', null);
INSERT INTO `t_group_member` VALUES ('7a2ca095df314b3e9090c865c118f212', '8843639c0da940e7a2add34b58c8bf48', '13fc7e3eb9a648a69e3ddf6fe538d725', 'n', '1', '1509430594338', null);

-- ----------------------------
-- Table structure for `t_group_type`
-- ----------------------------
DROP TABLE IF EXISTS `t_group_type`;
CREATE TABLE `t_group_type` (
  `id` varchar(256) NOT NULL,
  `group_type` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_group_type
-- ----------------------------
INSERT INTO `t_group_type` VALUES ('1', 'dota2');
INSERT INTO `t_group_type` VALUES ('2', '王者荣耀');
INSERT INTO `t_group_type` VALUES ('3', '纸牌');

-- ----------------------------
-- Table structure for `t_place`
-- ----------------------------
DROP TABLE IF EXISTS `t_place`;
CREATE TABLE `t_place` (
  `id` varchar(256) DEFAULT NULL,
  `place_name` varchar(256) DEFAULT NULL,
  `place_address` varchar(256) DEFAULT NULL,
  `place_type_id` varchar(256) DEFAULT NULL,
  `place_type` varchar(256) DEFAULT NULL,
  `place_phone` varchar(256) DEFAULT NULL,
  `gps_x` varchar(256) DEFAULT NULL,
  `gps_y` varchar(256) DEFAULT NULL,
  `creator_id` varchar(256) DEFAULT NULL,
  `create_time` varchar(256) DEFAULT NULL,
  `is_delete` varchar(8) DEFAULT NULL,
  `creator_name` varchar(256) DEFAULT NULL,
  `place_photo_urls` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_place
-- ----------------------------
INSERT INTO `t_place` VALUES ('1', '外婆家', '苏州职业技术学院', '2', '餐厅', '15509091212', '31.262390', '120.756401', '0', '1506063436067', 'n', 'admin', '[\"http://www.wonderfeng.xin/place/images/11.jpg\",\"http://www.wonderfeng.xin/place/images/22.jpg\",\"http://www.wonderfeng.xin/place/images/33.jpg\"]');
INSERT INTO `t_place` VALUES ('2', '辛香汇', '苏州职业技术学院A3', '1', '餐厅', '15509091213', '31.260910', '120.754130', '0', '1506063436067', 'n', 'admin', '[\"http://www.wonderfeng.xin/place/images/11.jpg\",\"http://www.wonderfeng.xin/place/images/22.jpg\",\"http://www.wonderfeng.xin/place/images/33.jpg\"]');
INSERT INTO `t_place` VALUES ('3', '腾龙', '苏州职业技术学院A', '3', '网吧', '15509091215', '31.259870', '120.753170', '0', '1506063436067', 'n', 'admin', '[\"http://www.wonderfeng.xin/place/images/11.jpg\",\"http://www.wonderfeng.xin/place/images/22.jpg\",\"http://www.wonderfeng.xin/place/images/33.jpg\"]');

-- ----------------------------
-- Table structure for `t_place_type`
-- ----------------------------
DROP TABLE IF EXISTS `t_place_type`;
CREATE TABLE `t_place_type` (
  `id` varchar(256) DEFAULT NULL,
  `place_type` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_place_type
-- ----------------------------
INSERT INTO `t_place_type` VALUES ('1', 'KTV');
INSERT INTO `t_place_type` VALUES ('2', '餐厅');
INSERT INTO `t_place_type` VALUES ('3', '网吧');
INSERT INTO `t_place_type` VALUES ('4', '桌游');

-- ----------------------------
-- Table structure for `t_user`
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` varchar(256) NOT NULL,
  `user_phone` varchar(32) DEFAULT NULL,
  `open_id` varchar(256) NOT NULL,
  `real_name` varchar(256) DEFAULT NULL,
  `nick_name` varchar(256) DEFAULT NULL,
  `gender` varchar(8) DEFAULT NULL,
  `avatarUrl` varchar(256) DEFAULT NULL,
  `language` varchar(64) DEFAULT NULL,
  `weixin_no` varchar(64) DEFAULT NULL,
  `country` varchar(64) DEFAULT NULL,
  `province` varchar(64) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `subscribe` varchar(8) DEFAULT NULL,
  `subscribe_time` varchar(64) DEFAULT NULL,
  `unionid` varchar(256) DEFAULT NULL,
  `remark` varchar(64) DEFAULT NULL,
  `groupid` varchar(64) DEFAULT NULL,
  `tagid_list` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('f8ef61a0b01d456dbeb61ae83f801838', null, 'oB0A_wmzVJlkX86VyPJbUYl2SNkM', null, 'wonderfeng', '1', 'http://wx.qlogo.cn/mmopen/HDt91gpRnn9TgVNCicT1wkRBkRHdZEibgabBMibuH7HiaaScPG5gBZnB3IVb6VS5jD58sVPRhfOnod0MdKCttxu0iafmC4zNCTmMx/0', 'zh_CN', null, '中国', '江苏', '苏州', '1', '1509423021', null, '', '0', '[]');
INSERT INTO `t_user` VALUES ('1846a6f0f12c4ebdbbf4cc951ae64c11', null, 'oxU8E0b59M5KtX9V0bjTqQ1dDp3k', null, '顾飞', '1', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJTxI2rXnYcn1HjTdWv8nq7vj29HeVgcKb0h61M8V7u7x0NthUutB0PaGtKC4vA8BJXkT5htjLNTA/0', null, null, 'China', 'Jiangsu', 'Suzhou', null, null, null, null, null, null);
INSERT INTO `t_user` VALUES ('a18dddbc4e9f49349c56e566f03439f6', null, 'oB0A_wi_XWV8GLcoKxI7yQF51Cdo', null, '张宇斌 天下常熟', '1', 'http://wx.qlogo.cn/mmopen/PiajxSqBRaEKBD4WxA9hia7ibC924jFQrU3UpPDYicjcHibyMFquwI5dicUDFAFsiaMabnGge2wWs6OozBNl1yIWuLlCA/0', 'zh_CN', null, '中国', '江苏', '苏州', '1', '1509430161', null, '', '0', '[]');
INSERT INTO `t_user` VALUES ('2dcb7aeb9fd247b7b581e668efea6b28', null, 'oxU8E0WsSLlEP0ibCNaQGs4bPyNg', null, '张宇斌 天下常熟', '1', 'https://wx.qlogo.cn/mmopen/vi_32/PiajxSqBRaELdwCRXOgM0ws8z1apF6z6306N8sCyS5F5X3ByeJqbLmA1ObePjI4m9RUn34LowibEOWEQuH98oq4Q/0', null, null, 'China', 'Jiangsu', 'Suzhou', null, null, null, null, null, null);
INSERT INTO `t_user` VALUES ('13fc7e3eb9a648a69e3ddf6fe538d725', null, 'oxU8E0VdtY9UrnabSmef376pNKF8', null, 'wonderfeng', '1', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIjcYSgojsmUMaNwUZK2DwicfEuVUkWjiaRWxT5IbiaolgLS4SOZVhBu60DjQdnr1cAzz7glicwo9jEOQ/0', null, null, 'China', 'Jiangsu', 'Suzhou', null, null, null, null, null, null);

-- ----------------------------
-- View structure for `v_group`
-- ----------------------------
DROP VIEW IF EXISTS `v_group`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_group` AS select `t_group`.`id` AS `id`,`t_group`.`group_name` AS `group_name`,`t_group`.`group_type_id` AS `group_type_id`,`t_group`.`group_type` AS `group_type`,`t_group`.`place_id` AS `place_id`,`t_group`.`create_time` AS `create_time`,`t_group`.`book_time` AS `book_time`,`t_group`.`deadline_time` AS `deadline_time`,`t_group`.`group_status` AS `group_status`,`t_group`.`max_num` AS `max_num`,`t_group`.`min_num` AS `min_num`,`t_group`.`duration` AS `duration`,`t_group`.`user_id` AS `user_id`,`t_group`.`remark` AS `remark`,`t_user`.`user_phone` AS `user_phone`,`t_user`.`open_id` AS `open_id`,`t_user`.`real_name` AS `real_name`,`t_user`.`nick_name` AS `nick_name`,`t_user`.`gender` AS `gender`,`t_user`.`avatarUrl` AS `avatarUrl`,`t_user`.`language` AS `language`,`t_user`.`weixin_no` AS `weixin_no`,`t_user`.`country` AS `country`,`t_user`.`province` AS `province`,`t_user`.`city` AS `city`,`t_place`.`place_name` AS `place_name`,`t_place`.`place_address` AS `place_address`,`t_place`.`place_type_id` AS `place_type_id`,`t_place`.`place_type` AS `place_type`,`t_place`.`place_phone` AS `place_phone`,`t_place`.`gps_x` AS `gps_x`,`t_place`.`gps_y` AS `gps_y`,`t_place`.`creator_id` AS `place_creator_id`,`t_place`.`create_time` AS `place_create_time`,`t_place`.`place_photo_urls` AS `place_photo_urls`,`t_place`.`is_delete` AS `is_delete`,`t_place`.`creator_name` AS `place_creator_name`,`t_user`.`subscribe` AS `subscribe`,`t_user`.`subscribe_time` AS `subscribe_time`,`t_user`.`unionid` AS `unionid`,`t_user`.`remark` AS `user_remark`,`t_user`.`groupid` AS `groupid`,`t_user`.`tagid_list` AS `tagid_list` from ((`t_group` join `t_user` on((`t_group`.`user_id` = `t_user`.`id`))) join `t_place` on((`t_group`.`place_id` = `t_place`.`id`))) ;

-- ----------------------------
-- View structure for `v_group_member`
-- ----------------------------
DROP VIEW IF EXISTS `v_group_member`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_group_member` AS select `t_group_member`.`id` AS `id`,`t_group_member`.`group_id` AS `group_id`,`t_group_member`.`user_id` AS `user_id`,`t_group_member`.`is_creator` AS `is_creator`,`t_group_member`.`op_status` AS `op_status`,`t_group_member`.`op_time` AS `op_time`,`t_group_member`.`remark` AS `remark`,`t_user`.`user_phone` AS `user_phone`,`t_user`.`open_id` AS `open_id`,`t_user`.`real_name` AS `real_name`,`t_user`.`nick_name` AS `nick_name`,`t_user`.`gender` AS `gender`,`t_user`.`avatarUrl` AS `avatarUrl`,`t_user`.`language` AS `language`,`t_user`.`weixin_no` AS `weixin_no`,`t_user`.`country` AS `country`,`t_user`.`province` AS `province`,`t_user`.`city` AS `city`,`t_user`.`subscribe` AS `subscribe`,`t_user`.`subscribe_time` AS `subscribe_time`,`t_user`.`unionid` AS `unionid`,`t_user`.`remark` AS `user_remark`,`t_user`.`groupid` AS `groupid`,`t_user`.`tagid_list` AS `tagid_list` from (`t_group_member` join `t_user` on((`t_group_member`.`user_id` = `t_user`.`id`))) ;

-- ----------------------------
-- View structure for `v_pre_group`
-- ----------------------------
DROP VIEW IF EXISTS `v_pre_group`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_pre_group` AS select `t_group`.`id` AS `id`,`t_group`.`deadline_time` AS `deadline_time`,`t_group`.`group_type` AS `group_type`,`t_group`.`duration` AS `duration`,`t_group`.`group_status` AS `group_status`,`t_group`.`book_time` AS `book_time` from `t_group` ;
