import com.shuimin.group.mid.redis.RedisUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pond.common.JSON;
import pond.common.S;
import redis.clients.jedis.*;

import java.util.*;

public class TestRedis {
    private Jedis jedis;

    @Before
    public void setJedis() {
        //连接redis服务器(在这里是连接本地的)
        jedis = RedisUtil.getJedis();
        //权限认证
        //jedis.auth("wf");
        System.out.println("连接服务成功");
    }

    /**
     * Redis操作字符串
     */
    @Test
    public void testString() {
        //添加数据
        jedis.set("name", "chx"); //key为name放入value值为chx
        System.out.println("拼接前:" + jedis.get("name"));//读取key为name的值

        //向key为name的值后面加上数据 ---拼接
        jedis.append("name", " is my name;");
        System.out.println("拼接后:" + jedis.get("name"));

        //删除某个键值对
        jedis.del("name");
        System.out.println("删除后:" + jedis.get("name"));

        //s设置多个键值对
        jedis.mset("name", "chenhaoxiang", "age", "20", "email", "chxpostbox@outlook.com");
        jedis.incr("age");//用于将键的整数值递增1。如果键不存在，则在执行操作之前将其设置为0。 如果键包含错误类型的值或包含无法表示为整数的字符串，则会返回错误。此操作限于64位有符号整数。
        System.out.println(jedis.get("name") + " " + jedis.get("age") + " " + jedis.get("email"));
    }
    @Test
    public void testMap1() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("name", JSON.stringify( pond.common.f.Tuple.pair("a","b")));
        map.put("age", JSON.stringify(pond.common.f.Tuple.pair("c","d")));
        map.put("email", JSON.stringify(pond.common.f.Tuple.pair("e","f")));
        jedis.hmset("user", map);
        Iterator<String> iterator = jedis.hkeys("user").iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            System.out.println(key + ":" + jedis.hmget("user", key));
        }
        jedis.del("user");
    }
    @Test
    public void testMap() {
        //添加数据
        Map<String, String> map = new HashMap<String, String>();
        map.put("name", "chx");
        map.put("age", "100");
        map.put("email", "***@outlook.com");
        jedis.hmset("user", map);
        //取出user中的name，结果是一个泛型的List
        //第一个参数是存入redis中map对象的key，后面跟的是放入map中的对象的key，后面的key是可变参数
        List<String> list = jedis.hmget("user", "name", "age", "email");
        System.out.println(list);

        //删除map中的某个键值
        jedis.hdel("user", "age");
        System.out.println("age:" + jedis.hmget("user", "age")); //因为删除了，所以返回的是null
        System.out.println("user的键中存放的值的个数:" + jedis.hlen("user")); //返回key为user的键中存放的值的个数2
        System.out.println("是否存在key为user的记录:" + jedis.exists("user"));//是否存在key为user的记录 返回true
        System.out.println("user对象中的所有key:" + jedis.hkeys("user"));//返回user对象中的所有key
        System.out.println("user对象中的所有value:" + jedis.hvals("user"));//返回map对象中的所有value

        //拿到key，再通过迭代器得到值
        Iterator<String> iterator = jedis.hkeys("user").iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            System.out.println(key + ":" + jedis.hmget("user", key));
        }
        jedis.del("user");
        System.out.println("删除后是否存在key为user的记录:" + jedis.exists("user"));//是否存在key为user的记录

    }

    @Test
    public void teststack(){

        //存放数据
        RedisUtil.pushStack("javaFramework","spring");
        RedisUtil.pushStack("javaFramework","springMVC");
        RedisUtil.pushStack("javaFramework","mybatis");
        S.echo(RedisUtil.queryAllKeysFromStack("javaFramework"));
//        System.out.println("javaFramework:"+jedis.lrange("javaFramework",0,2));
//        System.out.println("javaFramework:"+jedis.lindex("javaFramework",0));
//        System.out.println("javaFramework:"+jedis.lindex("javaFramework",1));
//        System.out.println("javaFramework:"+jedis.lrange("javaFramework",0,2));
//        System.out.println("javaFramework:"+jedis.lrange("javaFramework",0,2));
        S.echo(RedisUtil.queryKeysFromStack("javaFramework", 4, null));
        S.echo(RedisUtil.queryKeysFromStack("javaFramework", 3, ""));
        S.echo(RedisUtil.queryKeysFromStack("javaFramework", 2, ""));
        S.echo(RedisUtil.queryKeysFromStack("javaFramework", 1, ""));

        RedisUtil.delKey("javaFramework");
    }
    /**
     * jedis操作List
     */
    @Test
    public void testList(){
        //移除javaFramwork所所有内容
        jedis.del("javaFramwork");
        //存放数据
        jedis.lpush("javaFramework","spring");
        jedis.lpush("javaFramework","springMVC");
        jedis.lpush("javaFramework","mybatis");
        //取出所有数据,jedis.lrange是按范围取出
        //第一个是key，第二个是起始位置，第三个是结束位置
        System.out.println("长度:"+jedis.llen("javaFramework"));
        //System.out.println("lpop:"+ jedis.lpop("javaFramework"));
        //System.out.println("rpop:"+ jedis.rpop("javaFramework"));
        //jedis.llen获取长度，-1表示取得所有
        System.out.println("javaFramework:"+jedis.lrange("javaFramework",0,10));
//        jedis.rpop("javaFramework");
//        jedis.rpop("javaFramework");
//        System.out.println("javaFramework:"+jedis.exists("javaFramework"));
//        jedis.rpop("javaFramework");
//        System.out.println("javaFramework:"+jedis.exists("javaFramework"));


        jedis.del("javaFramework");
        System.out.println("删除后长度:"+jedis.llen("javaFramework"));
        System.out.println(jedis.lrange("javaFramework",0,-1));
    }

    /**
     * jedis操作Set
     */
    @Test
    public void testSortSet(){
        jedis.zadd("user", 217283,"1");
        jedis.zadd("user", new Date().getTime(),"2");
        jedis.zadd("user", new Date().getTime(),"3");
        jedis.zadd("user", new Date().getTime(),"4");
        jedis.zadd("user", new Date().getTime(),"5");
        S.echo("rank:",jedis.zrank("user","4"));
        S.echo("rank:",jedis.zrank("user","6"));
        S.echo("score:",jedis.zscore("user","4"));
        S.echo("score:",jedis.zscore("user","6"));
        Iterator<Tuple> iterator= jedis.zrangeByScoreWithScores("user",217283787,new Date().getTime()).iterator();

        while (iterator.hasNext()) {
            Tuple key = iterator.next();
            System.out.println("key:" +key.getElement()+":"+new Date((long) key.getScore()));
        }
        //移除user集合中的元素are

        jedis.del("user");
    }
    @Test
    public void testSet(){
        //添加
        jedis.sadd("user","chenhaoxiang");
        jedis.sadd("user","hu");
        jedis.sadd("user","chen");
        jedis.sadd("user","xiyu");
        jedis.sadd("user","chx");
        jedis.sadd("user","are");
        //移除user集合中的元素are
        jedis.srem("user","are");
        System.out.println("user中的value:"+jedis.smembers("user"));//获取所有加入user的value
        System.out.println("chx是否是user中的元素:"+jedis.sismember("user","chx"));//判断chx是否是user集合中的元素
        System.out.println("集合中的一个随机元素:"+jedis.srandmember("user"));//返回集合中的一个随机元素
        System.out.println("user中元素的个数:"+jedis.scard("user"));
        jedis.del("user");
    }

    /**
     * 排序
     */
    @Test
    public void test(){
        jedis.del("number");//先删除数据，再进行测试
        jedis.rpush("number","4");//将一个或多个值插入到列表的尾部(最右边)
        jedis.rpush("number","5");
        jedis.rpush("number","3");

        jedis.lpush("number","9");//将一个或多个值插入到列表头部
        jedis.lpush("number","1");
        jedis.lpush("number","2");
        System.out.println(jedis.lrange("number",0,jedis.llen("number")));
        System.out.println("排序:"+jedis.sort("number"));
        System.out.println(jedis.lrange("number",0,-1));//不改变原来的排序
        jedis.del("number");//测试完删除数据
    }

    @Test
    public void testScan(){
        for (int i=0 ; i<100; i++)
        {
            jedis.set("key"+i, "chx");
        }
        ScanParams scanParams = new ScanParams();
        scanParams.match("*7");

        String cursor = redis.clients.jedis.ScanParams.SCAN_POINTER_START;
        boolean cycleIsFinished = false;

        while (!cycleIsFinished) {
            ScanResult<String> scanResult = jedis.scan(cursor, scanParams);
            List<String> result = scanResult.getResult();
            S.echo("::",result);
            cursor = scanResult.getStringCursor();
            if (cursor.equals("0")) {
                cycleIsFinished = true;
            }
        }

        for (int i=0 ; i<100; i++)
        {
            jedis.del("key"+i);
        }

    }

    @Test
    public void testSScan(){
        for (int i=0 ; i<100; i++)
        {
            jedis.sadd("key", "chx"+i);
        }
        ScanParams scanParams = new ScanParams();
        scanParams.match("*");

        String cursor = redis.clients.jedis.ScanParams.SCAN_POINTER_START;
        boolean cycleIsFinished = false;

        while (!cycleIsFinished) {
            ScanResult<String> scanResult = jedis.sscan("key",cursor, scanParams);
            List<String> result = scanResult.getResult();
            S.echo("::",result);
            cursor = scanResult.getStringCursor();
            if (cursor.equals("0")) {
                cycleIsFinished = true;
            }
        }
        jedis.del("key");
    }
    @Test
    public void testMembers(){
        for (int i=0 ; i<10000; i++)
        {
            jedis.hset("key", "chx"+i, "pp"+i);
        }
        S.echo(RedisUtil.hscanFromKey("key").size());
        jedis.del("key");
    }
    @Test
    public void testHScan(){
        for (int i=0 ; i<10000; i++)
        {
            jedis.hset("key", "chx"+i, "pp"+i);
        }
        String cursor = redis.clients.jedis.ScanParams.SCAN_POINTER_START; // 返回0 说明遍历完成
        System.out.println("游标"+cursor);
        boolean cycleIsFinished = false;
        while (!cycleIsFinished) {
            ScanResult<Map.Entry<String, String>> hscanResult = jedis.hscan("key", cursor);
            List<Map.Entry<String, String>> scanResult = hscanResult.getResult();
            for(int m = 0;m < scanResult.size();m++){
                Map.Entry<String, String> mapentry  = scanResult.get(m);
                System.out.println("key: "+mapentry.getKey()+"  value: "+mapentry.getValue());
            }
            cursor = hscanResult.getStringCursor();
            if (cursor.equals("0")) {
                cycleIsFinished = true;
            }
        }
        jedis.del("key");
    }
    @Test
    public void testScanKey(){
        for (int i=0 ; i<100000; i++)
        {
            jedis.hset("key"+i, "chx"+i, "pp"+i);
        }
        long time = new Date().getTime();
        S.echo(RedisUtil.scanKey("*7"));
        long time1 = new Date().getTime();
        S.echo(time1-time);
        for (int i=0 ; i<100000; i++)
        {
            jedis.del("key"+i);
        }
    }
    @Test
    public void testKeys(){
        List<String> list = new ArrayList<>();
        for (int i=0 ; i<100; i++)
        {
            list.add("key"+i);
            jedis.hset("key"+i, "chx"+i, "pp"+i);
        }
        long time = new Date().getTime();
        S.echo(RedisUtil.keys("*7"));
        long time1 = new Date().getTime();
        S.echo(time1-time);
        S.echo( jedis.del(list.toArray(new String[list.size()])));

        //RedisUtil.del(list);
    }
    @After
    public void release() {
        S.echo("release resource");
        RedisUtil.returnResource(jedis);
    }

    @Test
    public void testTrans() {

        Transaction tx = jedis.multi();
        for (int i=0 ; i<100; i++)
        {
            tx.sadd("key", "chx"+i);
        }
        Response<Set<String>> list =tx.smembers("key");
        Response<Boolean> tmp =tx.exists("key");

        tx.del("key");
        tx.exec();

        S.echo("key:"+ list.get().toString());
        S.echo("key:"+ tmp.get());
        S.echo("key:"+jedis.exists("key"));
    }
    @Test
    public void id(){

    }
}
