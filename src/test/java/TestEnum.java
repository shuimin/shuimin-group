import com.shuimin.group.constant.GroupStatus;
import com.shuimin.group.constant.Gender;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;


public class TestEnum {
    @Test
    public void testGroupStatus(){

        LocalDateTime midnight = LocalDateTime.now().plusDays(1).withHour(0).withMinute(0).withSecond(0).withNano(0);
        long seconds = ChronoUnit.SECONDS.between(LocalDateTime.now(), midnight);
        System.out.println(seconds);



        Long time = new Long("1506658800000");
        System.out.println(new Date(time).toString());
         long date = new Date().getTime();
        System.out.println(date);
        System.out.println(new SimpleDateFormat("yyyy-mm-dd HH:mm:ss").format(new Date(date)));
        System.out.println(GroupStatus.get("create").val() +";"+ Gender.get(1).val());
    }
}
