import org.junit.Test;
import pond.common.S;
import pond.common.f.Callback;
import pond.common.f.Tuple;

import java.util.*;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicInteger;

import static pond.common.f.Tuple.pair;

public class TestTimer {

    public final static Map<String, Tuple<String, Integer>> usersAndScores =
            new ConcurrentSkipListMap<String, Tuple<String, Integer>>() {{
//                this.put("yxc", pair("Edwin yu", 100));
                this.put("wf", pair("Wang Feng", 100));
//                this.put("gf", pair("fly", 100));
//                this.put("rl", pair("gouzi", 100));
            }};

    public enum TimerEvents {
        EVENT_ALTER_SCORE(0),
        EVENT_ALTER_USERS(1),
        LOG(-1)
        ;
        final public int val;
        TimerEvents(int val){
            this.val = val;
        }
    }

    public static abstract class TimerEvent extends TimerTask {

        final TimerEvents type;
        final Callback<Map<String,Tuple<String, Integer>>> handler;

        TimerEvent(TimerEvents type, Callback<Map<String, Tuple<String, Integer>>> handler){
            this.type = type;
            this.handler = handler;
        }

        @Override
        public void run() {
//            Tuple<String, Integer> user = usersAndScores.get()

            System.out.println(type.toString());
            handler.apply(usersAndScores);
        }

    }

    public static class TimerEvent_Log extends TimerEvent{
        public TimerEvent_Log(){
            super(
                    TimerEvents.LOG,
                    users_sores -> {
                        S.echo(users_sores);
                    }
            );
        }
    }

    public static class TimerEvent_AddUser extends TimerEvent {

        public TimerEvent_AddUser(final AtomicInteger a) {
            super(
                    TimerEvents.EVENT_ALTER_USERS,
                    users_scores -> {
                        String id = "new" + a.getAndAdd(1);
                        String fullName =  Math.random() + "";
                        Integer score = 0;
                        S.echo("before add", id);
                        users_scores.put(id, pair(fullName, score));
                        S.echo("after add", users_scores);
                    }
            );
        }
    }

    public static class TimerEvent_RaiseOnesScore extends TimerEvent {

        public TimerEvent_RaiseOnesScore(String id, int delta) {
            super(
                    TimerEvents.EVENT_ALTER_SCORE,
                    users_scores -> {
                        Tuple<String, Integer> u = users_scores.get(id);
                        if(u != null){
                            Tuple<String, Integer> newUser = u.product(
                                    fullName -> fullName,
                                    score -> score + delta
                            );
                            S.echo("before delta", newUser);
                            users_scores.put(id, newUser);
                        }
                    }
            );
        }
    }


    public static class EventLoop {
        final Timer timer = new Timer();

    }

    public static void main(String[] args) throws Exception {

        Timer timer = new Timer();
        AtomicInteger sn = new AtomicInteger(0);

        timer.scheduleAtFixedRate(
                new TimerEvent_AddUser(sn),
                0,
                4000
        );

        timer.scheduleAtFixedRate(
                new TimerEvent_RaiseOnesScore("wf", 10),
                0,
                2000
        );

        timer.scheduleAtFixedRate(new TimerEvent_Log(),0, 2000);

//        long time_val = new Date().getTime();
//        System.out.println(time_val +"-------开始定时任务--------");
//        timer1(time_val+3000);
//        timer2();
//        timer3();
//        timer4();
//        timer5();
//        timer6();
    }

    // 第一种方法：指定任务task在指定时间time执行
//schedule(TimerTask task, Date time)
    public static void timer1(long time_val) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 11); // 控制时
        calendar.set(Calendar.MINUTE, 12);    // 控制分
        calendar.set(Calendar.SECOND, 12);    // 控制秒
        Date time = calendar.getTime();
//        Date time = new Date(time_val);
        Timer timer = new Timer();

        timer.schedule(new TimerTask() {
            public void run() {
                System.out.println(new Date().getTime()+"-------定时任务1--------");
            }
        }, time);
    }
}
