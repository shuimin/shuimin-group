package com.shuimin.group;

import com.shuimin.group.model.weixin.WeixinUser;
import com.shuimin.group.service.WeixinUserService;
import pond.web.Render;
import pond.web.Router;

public class WEIXIN_USER extends Router {
    {
        /* 获取用户信息 */
        get("/:open_id", (req, resp) -> {
            String openId = req.paramNonBlank("openId", "openId 不能为空");
            WeixinUser weixinUser = WeixinUserService.get(openId);
            if (weixinUser == null)
            {
                resp.send(404, openId+ "not found");
            }
            resp.render(Render.json(weixinUser));
        });
        /* 更新用户信息 */
        put("/:open_id", (req, resp) -> {
            String openId = req.paramNonBlank("open_id", "open_id 不能为空");
            WeixinUser weixinUser = WeixinUserService.get(openId);
            if (weixinUser == null)
            {
                resp.send(404, openId+ "not found");
            }
            weixinUser.merge(req.toMap());
            WeixinUserService.update( weixinUser);
            resp.send(200);
        });

    }
}