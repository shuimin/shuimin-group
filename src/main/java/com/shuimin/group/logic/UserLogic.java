package com.shuimin.group.logic;

import com.shuimin.group.constant.Config;
import com.shuimin.group.constant.UserRight;
import com.shuimin.group.mid.redis.RedisKey;
import com.shuimin.group.mid.redis.RedisService;
import com.shuimin.group.mid.redis.RedisUtil;
import com.shuimin.group.model.weixin.WeixinUser;
import pond.common.S;
import pond.common.STRING;
import pond.db.Record;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserLogic {
    public static String getUid(List<Record> uids)
    {
        S.echo("uid:"+uids);
        if (uids == null || uids.size() == 0 || uids.get(0).get("max_uid") == null)
        {
            return "0";
        }
        else
        {
            if(STRING.isBlank(uids.get(0).get("max_uid").toString()))
                return "0";
            else {
                return uids.get(0).get("max_uid").toString();
            }
        }
    }

    public static Map<String,String> transfer(WeixinUser user){
        S._assertNotNull(user);
        Map<String,String> map = new HashMap<>();

        if (user.containsKey("id") && null != user.get("id"))
        {
            map.put("id",user.get("id"));
        }
        if (user.containsKey("uid") && null != user.get("uid") )
        {
            map.put("uid",user.get("uid"));
        }
        if (user.containsKey("user_type") && null != user.get("user_type"))
        {
            map.put("user_type",user.get("user_type"));
            if ("1".equals(user.get("user_type")))
            {
                map.put(RedisKey.USER_DAY_CREATE_TIMES, String.valueOf(Config.SHOP_DAY_CREATE_TIMES));
            }
            else {
                map.put(RedisKey.USER_DAY_CREATE_TIMES, String.valueOf(Config.USER_DAY_CREATE_TIMES));
            }
            map.put(RedisKey.USER_DAY_JOIN_TIMES, String.valueOf(Config.USER_DAY_JOIN_TIMES));
            map.put(RedisKey.USER_DAY_CANCEL_TIMES, String.valueOf(Config.USER_DAY_CANCEL_TIMES));
            map.put(RedisKey.USER_DAY_LEAVE_TIMES, String.valueOf(Config.USER_DAY_LEAVE_TIMES));
        }
        if (user.containsKey("open_id") && null != user.get("open_id"))
        {
            map.put("open_id",user.get("open_id"));
        }
        if (user.containsKey("app_name") && null != user.get("app_name"))
        {
            map.put("app_name",user.get("app_name"));
        }
        if (user.containsKey("app_avatar_url") && null != user.get("app_avatar_url"))
        {
            map.put("app_avatar_url",user.get("app_avatar_url"));
        }
        return map;
    }

    public static boolean isHasRight(UserRight userRight, String userId){
        if (STRING.isBlank(userId) || !RedisService.exist(userId+RedisKey.USER))
        {
            return false;
        }
        if (new Integer(RedisUtil.hGetFieldVal(userId+RedisKey.USER, userRight.val())) >0 )
        {
            return true;
        }
        return false;
    }
}
