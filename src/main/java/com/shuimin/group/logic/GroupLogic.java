package com.shuimin.group.logic;

import com.shuimin.group.constant.Config;
import pond.common.S;


public class GroupLogic {
    public static boolean isNotifyDelay(long book_time, long delay)
    {
        if (delay >0)
        {
            return false;
        }
        if (S.now()+ Config.ms(Config.GROUP_NOTIFY_DELAY_TIME) >= book_time)
        {
            return true;
        }
        return false;
    }

    public static boolean isJoinEnd(long real_book_time)
    {
        if ((S.now()+ Config.ms(Config.GROUP_JOIN_END_TIME)) >= real_book_time)
        {
            return true;
        }
        return false;
    }
    public static boolean isCreateOk(long create_type, long max_num ,long join_num)
    {
        if (create_type ==0 && max_num == join_num)
        {
            return true;
        }
        if (create_type ==1 && (max_num +1) == join_num)
        {
            return true;
        }
        return false;
    }

    public static boolean isSendSign(long real_book_time)
    {
        if (S.now()+ Config.ms(Config.GROUP_SEND_SIGN_TIME) >= real_book_time )
            return true;
        return false;
    }

    public static boolean isCalcSign(long real_book_time)
    {
        if (S.now() >= (real_book_time + Config.ms(Config.GROUP_CALC_SIGN_TIME)))
            return true;
        return false;
    }
    public static boolean isSendFeedback(long real_book_time)
    {
        if (S.now() >= (real_book_time + Config.ms(Config.GROUP_SEND_FEEDBACK_TIME)))
            return true;
        return false;
    }
}
