package com.shuimin.group.logic;


/**
 * 地理相关运算（两个gps坐标之间距离等）
 * <p>
 * Created by sky on 2015/4/14.
 */
public class LocationLogic {

    private static final double EARTH_RADIUS = 6378137;

    /**
     * @param lng
     * @param lat
     * @param dis
     * @return [lng_min, lng_max, lat_min, lat_max]
     */
    public static double[] squarePoint(double lng, double lat, double dis) {
        double[] ret = new double[4];
        double dlng = 2 * Math.asin(Math.sin(dis / (2 * EARTH_RADIUS)) / Math.cos(deg2rad(lat)));
        dlng = rad2deg(dlng);

        double dlat = dis / EARTH_RADIUS;
        dlat = rad2deg(dlat);
        if (dlat < 0) {
            ret[2] = lat + dlat;
            ret[3] = lat - dlat;
        } else {
            ret[2] = lat - dlat;
            ret[3] = lat + dlat;
        }
        if (dlng < 0) {
            ret[0] = lng + dlng;
            ret[1] = lng - dlng;
        } else {
            ret[0] = lng - dlng;
            ret[1] = lng + dlng;
        }

        return ret;
    }

    //将角度转换为弧度
    public static double deg2rad(double degree) {
        return degree / 180 * Math.PI;
    }

    //将弧度转换为角度
    public static double rad2deg(double radian) {
        return radian * 180 / Math.PI;
    }


    public static double figuredMiles(double lng1,double lat1,double lng2,double lat2){
        double radLat1 = deg2rad(lat1);
        double radLat2 = deg2rad(lat2);
        double a = radLat1 - radLat2;
        double b = deg2rad(lng1) - deg2rad(lng2);
        double s = 2 * Math.asin(
                Math.sqrt(
                        Math.pow(Math.sin(a/2),2)
                                + Math.cos(radLat1)*Math.cos(radLat2)*Math.pow(Math.sin(b/2),2)
                )
        );
        s = s * EARTH_RADIUS;
        s = Math.round(s * 10000) / 10000;
        return s;
    };


}
