package com.shuimin.group.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pond.common.S;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Tool {
    public final static Logger logger = LoggerFactory.getLogger(Tool.class);
    public static boolean isClock0()
    {
        Calendar c = Calendar.getInstance();//可以对每个时间域单独修改

        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        if (hour ==0 && minute ==0)
        {
            return true;
        }
        return false;
    }
    public static String getDate(long time, String format)
    {
        S._assertNotNull(format);
        SimpleDateFormat df = new SimpleDateFormat( format );//设置日期格式 "yyyy-MM-dd HH:mm:ss"
        String date = df.format(new Date(time));
        logger.info(date);// new Date()为获取当前系统时间
        return date;
    }
}
