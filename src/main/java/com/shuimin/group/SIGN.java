package com.shuimin.group;

import com.shuimin.group.constant.Config;
import com.shuimin.group.logic.LocationLogic;
import com.shuimin.group.mid.redis.RedisKey;
import com.shuimin.group.mid.redis.RedisService;
import com.shuimin.group.mid.redis.RedisUtil;
import com.shuimin.group.model.place.Place;
import com.shuimin.group.model.sign.Sign;
import pond.common.S;
import pond.common.STRING;
import pond.web.Render;
import pond.web.Router;

import java.util.Map;

public class SIGN extends Router {
    {
        //签到与否
        get("/:group_id", (req, resp) -> {
            String userId = App.getUserId(req);
            String id = req.paramNonBlank("group_id", "id 不能为空");
            String groupKey = RedisService.getGroupKeyById(id);
            if (STRING.isBlank(groupKey))
            {
                resp.send(404, id+ " not found");
                return;
            }
            String signKey = RedisService.getKey(id, RedisKey.SIGN);
            Boolean exist = RedisUtil.sisMember(signKey,userId);
            resp.render(Render.text(exist.toString()));
        });

        //签到
        post("/:group_id", (req, resp) -> {
            String userId = App.getUserId(req);
            String id = req.paramNonBlank("group_id", "id 不能为空");
            String groupKey = RedisService.getGroupKeyById(id);
            if (STRING.isBlank(groupKey))
            {
                resp.send(404, id+ " not found");
                return;
            }
            String gps_x_str = req.param("gps_x");
            String gps_y_str = req.param("gps_y");
            if (STRING.isBlank(gps_x_str) || STRING.isBlank(gps_y_str))
            {
                resp.send(404, "gps params error");
                return;
            }
            double gps_x = new Double(gps_x_str);
            double gps_y =  new Double(gps_y_str);

            Map<String,String> groupMap = RedisUtil.hscanFromKey(groupKey);
            if (null == groupMap || groupMap.size() ==0)
            {
                resp.send(400, id+ " not existed");
                return;
            }

            double place_gps_x = new Double(groupMap.get("gps_x"));
            double place_gps_y = new Double(groupMap.get("gps_y"));
            double[] gps_ranges = LocationLogic.squarePoint(place_gps_x,place_gps_y, Config.SIGN_MAX_DISTANSE);
            if (gps_x < gps_ranges[0] || gps_x > gps_ranges[1] || gps_y < gps_ranges[2] || gps_y > gps_ranges[3] )
            {
                resp.send(403,  "请到活动场地签到，谢谢！");
                return;
            }

            //todo 是否增加签到时间限制

            String signKey = RedisService.getKey(id, RedisKey.SIGN);
            if (RedisUtil.sAdd(signKey,userId))
            {
                Sign sign = new Sign();
                sign.setId(S.uuid.vid());
                sign.set("weixin_user_id", userId);
                sign.set("group_id", id);
                sign.set("is_sign", "1");
                sign.set("sign_time", String.valueOf(S.now()));
                sign.set("is_handle", "0");
                App.DB.post(t->t.recordInsert(sign));
                resp.send(200, "sign ok");
            }
            else
            {
                resp.send(400, "sign failed");
            }
        });

    }
}
