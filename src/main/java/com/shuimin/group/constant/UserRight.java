package com.shuimin.group.constant;

import com.shuimin.group.mid.redis.RedisKey;

public enum UserRight {
    JOIN(RedisKey.USER_DAY_JOIN_TIMES),
    CREATE(RedisKey.USER_DAY_LEAVE_TIMES),
    LEAVE(RedisKey.USER_DAY_LEAVE_TIMES),
    CANCEL(RedisKey.USER_DAY_CANCEL_TIMES);
    private String val ;
    private UserRight( String val){
        this.val = val ;
    }
    public String val() {
        return this.val;
    }
    public static UserRight get(String val){
        for( UserRight userRight : UserRight.values()){
            if (userRight.val().equals(val))
                return userRight;
        }
        return null;
    }
}