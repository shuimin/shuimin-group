package com.shuimin.group.constant;

public enum GroupStatus {
    GROUP_STATUS_CREATE("create"),
    GROUP_STATUS_AUTO_CANCEL("auto_cancel"),
    GROUP_STATUS_MANUAL_CANCEL("manual_cancel"),
//    GROUP_STATUS_WAITING("waiting"),
//    GROUP_STATUS_DOING("doing"),
    GROUP_STATUS_FINISH("finish");
    private String status ;
    private GroupStatus( String groupStatus){
        this.status = groupStatus ;
    }
    public String val() {
        return this.status;
    }
    public static GroupStatus get(String val){
        for( GroupStatus status : GroupStatus.values()){
            if (status.val().equals(val))
                return status;
        }
        return null;
    }
}
