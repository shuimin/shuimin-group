package com.shuimin.group.constant;

public enum GroupLifeCycle {
    notify_delay("0"),
    join_end("1"),
    send_sign("2"),
    calc_sign("3"),
    send_feedback("4"),
    game_over("5");
    private String val ;
    private GroupLifeCycle(String val){
        this.val = val ;
    }
    public String val() {
        return this.val;
    }
    public static GroupLifeCycle get(String val){
        for( GroupLifeCycle status : GroupLifeCycle.values()){
            if (status.val().equals(val) )
                return status;
        }
        return null;
    }
}
