package com.shuimin.group.constant;

public enum Gender {
    UNKNOWN(0),
    FEMALE(1),
    MALE(2);
    private int value ;
    private Gender(int value){
        this.value = value ;
    }
    public int val() {
        return this.value;
    }
    public static Gender get(int val){
        for( Gender status : Gender.values()){
            if (status.val()== val)
                return status;
        }
        return null;
    }
}
