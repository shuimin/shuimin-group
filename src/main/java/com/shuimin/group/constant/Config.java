package com.shuimin.group.constant;

public class Config {
    public static final long GROUP_JOIN_END_TIME = 25;//    加入截止时间
    public static final long GROUP_NOTIFY_DELAY_TIME = 30;//  通知延时时间
    public static final long GROUP_CREATE_MIN_TIME = 30;// 创建最短时间
    public static final long GROUP_CREATE_MAX_TIME = 120;//创建最长时间
    public static final long GROUP_SEND_SIGN_TIME = 5;// 发送签到消息时间
    public static final long GROUP_CALC_SIGN_TIME = 5;// 计算签到时间
    public static final long GROUP_SEND_FEEDBACK_TIME = 30;// 发送反馈消息时间

    public static long ms(long minute)
    {
        return minute*60*1000;
    }


    public static final long USER_DAY_JOIN_TIMES = 3;//
    public static final long USER_DAY_CREATE_TIMES = 2;//
    public static final long USER_DAY_LEAVE_TIMES = 2;
    public static final long USER_DAY_CANCEL_TIMES = 2;
    public static final long SHOP_DAY_CREATE_TIMES = 5;

    public static final double SIGN_MAX_DISTANSE = 500;
}
