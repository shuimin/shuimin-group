package com.shuimin.group;

import com.shuimin.group.model.weixin.WeixinUser;
import com.shuimin.group.model.follow.FollowGroupType;
import com.shuimin.group.model.follow.FollowPlace;
import com.shuimin.group.model.follow.FollowUser;
import com.shuimin.group.model.group.GroupType;
import com.shuimin.group.model.place.Place;
import com.shuimin.group.service.FollowService;
import com.shuimin.group.service.PlaceService;
import com.shuimin.group.service.WeixinUserService;
import pond.common.S;
import pond.web.Render;
import pond.web.Router;
import pond.web.Session;

public class FOLLOW extends Router {
    {
        /* 查询关注的用户 */
        get("/user", (req, resp) -> {
            WeixinUser weixinUser = Session.get(req).get(App.APP_WEIXIN_USER_INFO);
            String userId = weixinUser.id();
            App.DB.post(
                    t->{
                        resp.render(
                            Render.json(
                                t.query(WeixinUser.class, FollowService.allUser(userId)))
                        );
                    }
            );
        });

        /* 查询用户具体信息 */
        get("/user/:id", (req, resp) -> {
            String id = req.paramNonBlank("id", "id 不能为空");
            resp.render(Render.json(App.DB.get(t -> t.recordById(WeixinUser.class, id))));
        });


        /* 关注某人 */
        post("/user/:id", (req, resp) -> {
            String followUserId = req.param("id");
            WeixinUser weixinUser = Session.get(req).get(App.APP_WEIXIN_USER_INFO);
            String userId = weixinUser.id();

            if (!WeixinUserService.isExists(followUserId))
            {
                resp.send(404, followUserId+ " not found");
                return;
            }
            FollowUser followUser = new FollowUser();
            followUser.setId(S.uuid.vid());
            followUser.set("user_id",  userId);
            followUser.set("follow_user_id", followUserId);
            followUser.set("is_delete","n");
            App.DB.post(t->t.recordInsert(followUser));
            resp.send(200);
        });
        del("/user/:id", (req, resp) -> {
            String followUserid = req.paramNonBlank("id", "id 不能为空");
            WeixinUser weixinUser = Session.get(req).get(App.APP_WEIXIN_USER_INFO);
            String userId = weixinUser.id();
            App.DB.post(
                    t->{
                        FollowUser followUser = FollowService.getFollowUser(userId,followUserid);
                        if (followUser == null)
                        {
                            resp.send(200);
                            return;
                        }
                        followUser.set("is_delete","y");
                        t.recordUpdate(followUser);
                        resp.send(200);
                    }
            );

        });
        /* 查询场地 */
        get("/place", (req, resp) -> {
            WeixinUser weixinUser = Session.get(req).get(App.APP_WEIXIN_USER_INFO);
            String userId = weixinUser.id();
            App.DB.post(
                    t->{
                        resp.render(Render.json(
                                t.query(Place.class, FollowService.allPlace(userId)))
                        );
                    }
            );
        });

        /* 查询场地具体信息 */
        get("/place/:id", (req, resp) -> {
            String id = req.paramNonBlank("id", "id 不能为空");
            resp.render(Render.json(App.DB.get(t -> t.recordById(Place.class, id))));
        });

        /* 关注场地 */
        post("/place/:id", (req, resp) -> {
            App.DB.post(
                    t->{
                        String placeId = req.param("id");
                        WeixinUser weixinUser = Session.get(req).get(App.APP_WEIXIN_USER_INFO);
                        String userId = weixinUser.id();

                        if (!PlaceService.isExists(t,placeId))
                        {
                            resp.send(404, placeId+ " not found");
                            return;
                        }
                        FollowPlace followPlace = new FollowPlace();
                        followPlace.setId(S.uuid.vid());
                        followPlace.set("user_id",  userId);
                        followPlace.set("place_id", placeId);
                        followPlace.set("is_delete","n");
                        t.recordInsert(followPlace);
                        resp.send(200);
                    }
            );
        });
        del("/place/:id", (req, resp) -> {
            String placeId = req.paramNonBlank("id", "id 不能为空");
            WeixinUser weixinUser = Session.get(req).get(App.APP_WEIXIN_USER_INFO);
            String userId = weixinUser.id();
            App.DB.post(
                    t->{
                        FollowPlace followPlace = FollowService.getFollowPlace(userId,placeId);
                        if (followPlace == null)
                        {
                            resp.send(200);
                            return;
                        }
                        followPlace.set("is_delete","y");
                        t.recordUpdate(followPlace);
                        resp.send(200);
                    }
            );

        });
        /* 查询局类型 */
        get("/group_type", (req, resp) -> {
            App.DB.post(
                    t->{
                        WeixinUser weixinUser = Session.get(req).get(App.APP_WEIXIN_USER_INFO);
                        String userId = weixinUser.id();
                        resp.render(Render.json(t.query(FollowGroupType.class, FollowService.allFollowGroupType(userId))));
                    }
            );
        });

        /* 关注局类型 */
        post("/group_type/:id", (req, resp) -> {
            App.DB.post(
                    t->{
                        String groupTypeId = req.param("id");
                        WeixinUser weixinUser = Session.get(req).get(App.APP_WEIXIN_USER_INFO);
                        String userId = weixinUser.id();

                        GroupType groupType = t.recordById(GroupType.class, groupTypeId);
                        if (groupType ==null)
                        {
                            resp.send(404, groupTypeId+ " not found");
                            return;
                        }
                        FollowGroupType followGroupType = new FollowGroupType();
                        followGroupType.setId(S.uuid.vid());
                        followGroupType.set("user_id",  userId);
                        followGroupType.set("group_type_id", groupType.id());
                        followGroupType.set("group_type", groupType.get("group_type"));
                        followGroupType.set("is_delete","n");
                        t.recordInsert(followGroupType);
                        resp.send(200);
                    }
            );
        });

        del("/group_type/:id", (req, resp) -> {
            String groupTypeId = req.paramNonBlank("id", "id 不能为空");
            WeixinUser weixinUser = Session.get(req).get(App.APP_WEIXIN_USER_INFO);
            String userId = weixinUser.id();
            App.DB.post(
                    t->{
                        FollowGroupType followGroupType = FollowService.getFollowGroupType(userId,groupTypeId);
                        if (followGroupType == null)
                        {
                            resp.send(200);
                            return;
                        }
                        followGroupType.set("is_delete","y");
                        t.recordUpdate(followGroupType);
                        resp.send(200);
                    }
            );

        });

    }
}
