package com.shuimin.group.model.shop;

import pond.db.Model;

public class Shop extends Model {
    {
        table("t_shop");
        id("id");
        field("weixin_user_id");
        field("weixin_uid");
        field("place_id");
        field("shop_owner_name");
        field("shop_owner_phone");
        field("shop_owner_id");
        field("shop_owner_email");
        field("shop_business_photo");
    }
}
