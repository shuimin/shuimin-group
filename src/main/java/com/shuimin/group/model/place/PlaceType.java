package com.shuimin.group.model.place;

import pond.db.Model;

public class PlaceType extends Model {
    {
        table("t_place_type");
        id("id");
        field("place_type");
    }
}
