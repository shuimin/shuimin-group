package com.shuimin.group.model.place;

import pond.db.Model;

public class PlacePic extends Model{
    {
        {
            table("t_place_pic");
            id("id");
            field("place_id");
            field("place_pic_type");
            field("place_pic_url");
            field("upload_user_name");
            field("upload_time");
        }
    }
}
