package com.shuimin.group.model.place;

import pond.db.Model;

public class PlaceGroupType extends Model {
    {
        table("r_place_group_type");
        id("id");
        field("group_type_id");
        field("group_type");
        field("place_id");
    }
}