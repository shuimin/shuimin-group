package com.shuimin.group.model.place;
import pond.db.Model;
import java.util.HashMap;
import java.util.Map;

public class Place extends Model {
    {
        table("t_place");
        id("id");
        field("place_name");
        field("place_address");
        field("place_type_id");
        field("place_type");
        field("place_phone");
        field("gps_x");
        field("gps_y");
        field("creator_id");
        field("creator_name");
        field("create_time");
        field("place_photo_url");
        field("is_delete");
        field("place_cell_phone");
        field("place_backup_phones");
    }
    public Map<String, String> toJedisMap() {
        Map<String, String> ret = new HashMap<>();
        for (String s : this.fields()) {
            if (null != this.get(s)) {
                ret.put(s, this.get(s));
            }
        }
        return ret;
    }


}