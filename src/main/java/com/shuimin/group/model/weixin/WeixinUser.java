package com.shuimin.group.model.weixin;

import pond.db.Model;

import java.util.HashMap;
import java.util.Map;

public class WeixinUser extends Model {
    {
        table("t_weixin_user");
        id("id");
        field("uid");
        field("user_type");
        field("user_phone");
        field("open_id");
        field("app_name");
        field("app_avatar_url");
        field("nick_name");
        field("gender");
        field("avatarUrl");
        field("language");

        field("country");
        field("province");
        field("city");

        field("subscribe");
        field("subscribe_time");
        field("unionid");
        field("remark");
        field("groupid");
        field("tagid_list");
    }
    public Map<String, String> toJedisMap() {
        Map<String, String> ret = new HashMap<>();
        for (String s : this.fields()) {
            if (null != this.get(s)) {
                ret.put(s, this.get(s));
            }
        }
        return ret;
    }
}
