package com.shuimin.group.model.group;

import pond.db.Model;

public class GroupMember extends Model {
    {
        table("t_group_member");
        id("id");
        field("group_id");
        field("user_id");
        field("is_creator");
        field("op_status");
        field("op_time");
    }
}