package com.shuimin.group.model.group;

import pond.db.Model;

public class GroupType extends Model {
    {
        table("t_group_type");
        id("id");
        field("group_type");
    }
}