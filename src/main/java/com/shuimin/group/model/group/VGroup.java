package com.shuimin.group.model.group;

import com.shuimin.group.model.weixin.WeixinUser;
import com.shuimin.group.model.place.Place;
import pond.common.S;
import pond.common.STRING;
import pond.db.Model;

import java.util.HashMap;
import java.util.Map;

public class VGroup extends Model {
    {
        table("v_group");
        /* t_group */
        id("id");
        field("group_name"); //ok
        field("group_type");
        field("group_type_id"); //ok
        field("place_id");    //ok
        field("create_time"); //ok
        field("book_time");  //ok
        field("group_status");
        field("max_num"); //ok
        field("user_id");
        field("remark");
        field("create_type");
        field("delay");

        /*  user */
        field("user_phone");
        field("open_id");
        field("app_name");
        field("nick_name");
        field("gender");
        field("avatarUrl");
        field("app_avatar_url");
        field("uid");
        field("user_type");

        /* t_place */
        field("place_name");
        field("place_address");
        field("place_type_id");
        field("place_type");
        field("place_phone");
        field("gps_x");
        field("gps_y");
        field("place_creator_id");
        field("place_creator_name");
        field("place_create_time");
        field("place_photo_url");
        field("is_delete");
        field("place_cell_phone");
        field("place_backup_phones");
    }
    public Map<String, String> toJedisMap() {
        Map<String, String> ret = new HashMap<>();
        for (String s : this.fields()) {
            if (null != this.get(s)) {
                ret.put(s, this.get(s));
            }
        }
        return ret;
    }
    public Group group() {
        Group group= new Group();
        group.setId(this.get("id"));
        if (STRING.isBlank(group.id()))
            group.setId(S.uuid.vid());
        group.set("group_name", this.get("group_name"));
        group.set("group_type", this.get("group_type"));
        group.set("group_type_id", this.get("group_type_id"));
        group.set("place_id", this.get("place_id"));
        group.set("create_time", this.get("create_time"));
        group.set("book_time", this.get("book_time"));
        group.set("group_status", this.get("group_status"));
        group.set("max_num", this.get("max_num"));
        group.set("user_id", this.get("user_id"));
        group.set("remark", this.get("remark"));
        group.set("create_type", this.get("create_type"));
        group.set("delay", this.get("delay"));
        return group;
    }
    public WeixinUser user() {
        WeixinUser weixinUser = new WeixinUser();
        weixinUser.setId(this.get("user_id"));
        if (STRING.isBlank(weixinUser.id()))
            weixinUser.setId(S.uuid.vid());
        weixinUser.set("user_phone", this.get("user_phone"));
        weixinUser.set("open_id", this.get("open_id"));
        weixinUser.set("app_avatar_url", this.get("app_avatar_url"));
        weixinUser.set("app_name", this.get("app_name"));
        weixinUser.set("nick_name", this.get("nick_name"));
        weixinUser.set("gender", this.get("gender"));
        weixinUser.set("avatarUrl", this.get("avatarUrl"));
        weixinUser.set("uid", this.get("uid"));
        weixinUser.set("user_type", this.get("user_type"));

        weixinUser.set("subscribe", this.get("subscribe"));
        weixinUser.set("subscribe_time", this.get("subscribe_time"));
        weixinUser.set("unionid", this.get("unionid"));
        weixinUser.set("remark", this.get("user_remark"));
        weixinUser.set("groupid", this.get("groupid"));
        weixinUser.set("tagid_list", this.get("tagid_list"));
        return weixinUser;
    }
    public Place place() {
        Place place= new Place();
        place.setId(this.get("place_id"));
        if (STRING.isBlank(place.id()))
            place.setId(S.uuid.vid());
        place.set("place_name", this.get("place_name"));
        place.set("place_address", this.get("place_address"));
        place.set("place_type_id", this.get("place_type_id"));
        place.set("place_type", this.get("place_type"));
        place.set("place_phone", this.get("place_phone"));
        place.set("gps_x", this.get("gps_x"));
        place.set("gps_y", this.get("gps_y"));
        place.set("creator_id", this.get("place_creator_id"));
        place.set("creator_name", this.get("place_creator_name"));
        place.set("create_time", this.get("place_create_time"));
        place.set("place_photo_url", this.get("place_photo_url"));
        place.set("is_delete", this.get("is_delete"));
        place.set("place_cell_phone", this.get("place_cell_phone"));
        place.set("place_backup_phones", this.get("place_backup_phones"));
        return place;
    }

}
