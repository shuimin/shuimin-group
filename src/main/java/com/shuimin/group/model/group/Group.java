package com.shuimin.group.model.group;

import pond.db.Model;

import java.util.HashMap;
import java.util.Map;

public class Group extends Model {
    {
        table("t_group");
        id("id");
        field("group_name"); //ok
        field("group_type");
        field("group_type_id"); //ok
        field("place_id");    //ok
        field("create_time"); //ok
        field("book_time");  //ok
        field("group_status");
        field("max_num"); //ok
        field("user_id");
        field("remark");
        field("create_type");
        field("delay");
    }
    public Map<String, String> toJedisMap() {
        Map<String, String> ret = new HashMap<>();
        for (String s : this.fields()) {
            if (null != this.get(s)) {
                ret.put(s, this.get(s));
            }
        }
        return ret;
    }
}