package com.shuimin.group.model.group;
import com.shuimin.group.model.weixin.WeixinUser;
import com.shuimin.group.model.place.Place;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupInMem {
    private String id;
    private Group group;
    private WeixinUser weixinUser;
    private Place place;
    private List<VGroupMember> vGroupMemberList;
    private Map<String,VGroupMember> vGroupMemberMap;// user_id : VGroupMember
    private boolean handle;

    public Map<String, VGroupMember> getvGroupMemberMap() {
        return vGroupMemberMap;
    }

    public void setvGroupMemberMap(Map<String, VGroupMember> vGroupMemberMap) {
        this.vGroupMemberMap = vGroupMemberMap;
    }

    public boolean isHandle() {
        return handle;
    }

    public void setHandle(boolean handle) {
        this.handle = handle;
    }

    public GroupInMem (VGroup vGroup, List<VGroupMember> vGroupMemberList){
        this.id = vGroup.id();
        this.group = vGroup.group();
        this.weixinUser = vGroup.user();
        this.place = vGroup.place();
        this.vGroupMemberList = vGroupMemberList;
        this.vGroupMemberMap = new HashMap<>();
        this.handle = false;
        handleData();
    }
    public void handleData(){
        if (null == vGroupMemberList) {
            return;
        }
        if (null == vGroupMemberMap)
        {
            vGroupMemberMap = new HashMap<>();
        }
        for (int i=0; i < vGroupMemberList.size(); i++)
        {
            VGroupMember vGroupMember = vGroupMemberList.get(i);
            String key = vGroupMember.get("user_id");
            if (vGroupMemberMap.containsKey(key))
            {
                VGroupMember vGroupMemberForMap = vGroupMemberMap.get(key);
                if (new Long(vGroupMemberForMap.get("op_time").toString()).longValue() < new Long(vGroupMember.get("op_time").toString()).longValue()  )
                {
                    vGroupMemberForMap.set("op_time", vGroupMember.get("op_time").toString());
                }
                String op_status = new Integer(new Integer(vGroupMemberForMap.get("op_status")).intValue()
                        + new Integer(vGroupMember.get("op_status")).intValue()).toString();
                vGroupMemberForMap.set("op_status",op_status);
                vGroupMemberMap.replace(key,vGroupMemberForMap);
            }
            else
            {
                vGroupMemberMap.put(key,vGroupMember);
            }
        }
        vGroupMemberList = new ArrayList<>(vGroupMemberMap.values()) ;
        this.handle = true;
        return;
    }
    public Group getGroup() {
        return group;
    }
    public void setGroup(Group group) {
        this.group = group;
    }
    public WeixinUser getWeixinUser() {
        return weixinUser;
    }
    public void setWeixinUser(WeixinUser weixinUser) {
        this.weixinUser = weixinUser;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<VGroupMember> getvGroupMemberList() {
        return vGroupMemberList;
    }

    public void setvGroupMemberList(List<VGroupMember> vGroupMemberList) {
        this.vGroupMemberList = vGroupMemberList;
    }

}
