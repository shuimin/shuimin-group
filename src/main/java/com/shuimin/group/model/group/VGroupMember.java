package com.shuimin.group.model.group;

import com.shuimin.group.model.weixin.WeixinUser;
import pond.common.S;
import pond.common.STRING;
import pond.db.Model;

public class VGroupMember extends Model {
    {
        table("v_group_member");
        /* t_group */
        id("id");
        field("group_id");
        field("user_id");
        field("is_creator");
        field("op_status");
        field("op_time");

        /*  user */
        field("user_phone");
        field("open_id");
        field("app_name");
        field("nick_name");
        field("gender");
        field("avatarUrl");
        field("app_avatar_url");
        field("uid");
        field("user_type");

    }
    public GroupMember groupMember() {
        GroupMember groupMember = new GroupMember();
        groupMember.setId(this.id());
        if (STRING.isBlank(groupMember.id()))
            groupMember.setId(S.uuid.vid());
        groupMember.set("group_id", this.get("group_id"));
        groupMember.set("user_id", this.get("user_id"));
        groupMember.set("is_creator", this.get("is_creator"));
        groupMember.set("op_status", this.get("op_status"));
        groupMember.set("op_time", this.get("op_time"));
        return groupMember;
    }
    public WeixinUser user() {
        WeixinUser weixinUser = new WeixinUser();
        weixinUser.setId(this.get("user_id"));
        if (STRING.isBlank(weixinUser.id()))
            weixinUser.setId(S.uuid.vid());
        weixinUser.set("user_phone", this.get("user_phone"));
        weixinUser.set("open_id", this.get("open_id"));
        weixinUser.set("app_avatar_url", this.get("app_avatar_url"));
        weixinUser.set("app_name", this.get("app_name"));
        weixinUser.set("nick_name", this.get("nick_name"));
        weixinUser.set("gender", this.get("gender"));
        weixinUser.set("avatarUrl", this.get("avatarUrl"));
        weixinUser.set("uid", this.get("uid"));
        weixinUser.set("user_type", this.get("user_type"));

        return weixinUser;
    }
    public void setUser(WeixinUser weixinUser){
        if (null == weixinUser)
            return;
        if (null != weixinUser.id())
            this.set("user_id", weixinUser.id());
        if (null != weixinUser.get("user_phone"))
            this.set("user_phone", weixinUser.get("user_phone"));
        if (null != weixinUser.get("open_id"))
            this.set("open_id", weixinUser.get("open_id"));
        if (null != weixinUser.get("app_name"))
            this.set("app_name", weixinUser.get("app_name"));
        if (null != weixinUser.get("app_avatar_url"))
            this.set("app_avatar_url", weixinUser.get("app_avatar_url"));
        if (null != weixinUser.get("nick_name"))
            this.set("nick_name", weixinUser.get("nick_name"));
        if (null != weixinUser.get("gender"))
            this.set("gender", weixinUser.get("gender"));
        if (null != weixinUser.get("avatarUrl"))
            this.set("avatarUrl", weixinUser.get("avatarUrl"));
        if (null != weixinUser.get("uid"))
            this.set("uid", weixinUser.get("uid"));
        if (null != weixinUser.get("user_type"))
            this.set("user_type", weixinUser.get("user_type"));

    }
}