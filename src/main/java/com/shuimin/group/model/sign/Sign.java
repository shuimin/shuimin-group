package com.shuimin.group.model.sign;

import pond.db.Model;

public class Sign extends Model {
    {
        table("t_sign");
        id("id");
        field("weixin_user_id");
        field("group_id");
        field("is_sign");
        field("sign_time");
        field("is_handle");
    }
}
