package com.shuimin.group.model.follow;

import pond.db.Model;

public class FollowGroupType extends Model {
    {
        table("t_follow_group_type");
        id("id");
        field("group_type_id");
        field("group_type");
        field("user_id");
        field("is_delete");
    }
}