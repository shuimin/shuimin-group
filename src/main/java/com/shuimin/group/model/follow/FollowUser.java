package com.shuimin.group.model.follow;

import pond.db.Model;

public class FollowUser  extends Model {
    {
        table("t_follow_user");
        id("id");
        field("follow_user_id");
        field("user_id");
        field("is_delete");
    }
}