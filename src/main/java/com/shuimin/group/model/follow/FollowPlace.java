package com.shuimin.group.model.follow;

import pond.db.Model;

public class FollowPlace extends Model {
    {
        table("t_follow_place");
        id("id");
        field("place_id");
        field("user_id");
        field("is_delete");
    }
}
