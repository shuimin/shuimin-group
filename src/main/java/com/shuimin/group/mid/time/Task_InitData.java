package com.shuimin.group.mid.time;

import com.shuimin.group.App;
import com.shuimin.group.constant.GroupLifeCycle;
import com.shuimin.group.logic.UserLogic;
import com.shuimin.group.mid.redis.RedisKey;
import com.shuimin.group.mid.redis.RedisService;
import com.shuimin.group.mid.redis.RedisUtil;
import com.shuimin.group.model.group.GroupInMem;
import com.shuimin.group.model.group.VGroupMember;
import com.shuimin.group.model.place.Place;
import com.shuimin.group.model.weixin.WeixinUser;
import com.shuimin.group.service.GroupService;
import pond.common.S;
import pond.common.STRING;
import pond.db.Record;
import pond.db.sql.Criterion;
import pond.db.sql.Sql;
import pond.db.sql.SqlSelect;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;

public class Task_InitData extends TimerTask {
    @Override
    public void run() {
        S.echo("init data");

        List<Record> uids = App.DB.get(t->t.query("select max(uid) as max_uid from t_weixin_user;"));
        RedisUtil.set(RedisKey.WEIXIN_USER_UID, UserLogic.getUid(uids));

        Map<String,Map<String,String>> initDataMap = new HashMap<>();
        //存储地点
        List<Place> placeList =  App.DB.get(t-> t.query(Place.class,  Sql.select("*").from("t_place")));
        S._for(placeList).each(item ->{
            initDataMap.put(item.id()+ RedisKey.PLACE, item.toJedisMap());
        });

        //todo 获取数据库中的sign  is_handle =0 的数据，更新到Redis


        //todo 用户可以加入，退出，组建次数
        List<WeixinUser> weixinUserList =  App.DB.get(t-> t.query(WeixinUser.class,  Sql.select("*").from("t_weixin_user")));
        S._for(weixinUserList).each(item ->{
            initDataMap.put(item.id()+ RedisKey.USER, UserLogic.transfer(item));

        });
        S.echo("weixinUserList:"+ weixinUserList.size());
        //RedisUtil.hAdd(initDataMap);

        //存储创建的局,以及成员 1. 0:* 1:*   2. // create_type:place_id:time:delay:group_id (group_type 放在二级查询)
        S._for(GroupService.createAllVGroup()).each(item ->{
            String groupKey = RedisService.getGroupKey(item);
            if (STRING.notBlank(groupKey))
            {
                Map<String,String> groupValMap = item.toJedisMap();
                groupValMap.put(RedisKey.GROUP_LIFE_CYCLE, GroupLifeCycle.notify_delay.val());
                initDataMap.put(groupKey,groupValMap);
                SqlSelect memberSql = Sql.select("*").from("v_group_member")
                        .where("group_id", Criterion.EQ, item.id());
                List<VGroupMember> vGroupMemberList = App.DB.get(t-> t.query(VGroupMember.class, memberSql));
                GroupInMem groupInMem = new GroupInMem(item, vGroupMemberList);
                //group_id:user_id:member  [name:  jointime, creator]
                S._for(groupInMem.getvGroupMemberList()).each(member->{
                    String memberkey = RedisService.getMemberKey(item.id(), member.get("user_id"));
                    initDataMap.put(memberkey, RedisService.getMemberValue(member));
                });
            }
        });

        S.echo("data:"+initDataMap);
        RedisUtil.hAdd(initDataMap);


    }
}