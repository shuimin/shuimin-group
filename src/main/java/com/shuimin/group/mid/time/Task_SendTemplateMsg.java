package com.shuimin.group.mid.time;

import com.shuimin.group.mid.message.MessageService;
import com.shuimin.group.mid.message.SendType;
import com.shuimin.group.mid.message.TemplateMessage;
import com.shuimin.group.mid.redis.RedisKey;
import com.shuimin.group.mid.redis.RedisService;
import com.shuimin.group.mid.redis.RedisUtil;
import com.shuimin.group.service.WeixinService;
import pond.common.S;
import pond.common.STRING;


import java.util.*;

public class Task_SendTemplateMsg extends TimerTask{

    @Override
    public void run() {
        String msgKey = RedisUtil.dequeue(RedisKey.TO_DO_TEMPLATE_MSG);
        List<String> keyList = new ArrayList<>();
        int max = 10000;
        int num = 0;
        while (STRING.notBlank(msgKey))
        {
            keyList.add(msgKey);
            num ++;
            if (num >= max)
            {
                break;
            }
            msgKey = RedisUtil.dequeue(RedisKey.TO_DO_TEMPLATE_MSG);
        }
        S._for(keyList).each(key->{
            Map<String,String> msgMap = RedisUtil.hscanFromKey(key);
            String type = msgMap.get("type");
            String group_id = msgMap.get("group_id");
            String send_type = msgMap.get("send_type");
            if ( STRING.isBlank(type) || STRING.isBlank(group_id) || STRING.isBlank(send_type))
            {
                return;
            }
            if (SendType.MULTIPLE == SendType.get(send_type))
            {
                MessageService.sendTemplateMsgToMulti(msgMap);
            }
            else
            {
                MessageService.sendTemplateMsgToSingle(msgMap);
            }
        });
    }
}
