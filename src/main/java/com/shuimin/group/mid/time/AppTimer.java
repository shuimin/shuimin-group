package com.shuimin.group.mid.time;
import com.shuimin.group.mid.redis.RedisUtil;
import pond.common.S;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class AppTimer {
    public final static ScheduledExecutorService scheduledExecutorService
            = Executors.newScheduledThreadPool(
            Runtime.getRuntime().availableProcessors() + 1
    );
    public static void init(){
        scheduledExecutorService.schedule(new Task_InitData(),0, TimeUnit.SECONDS);
        scheduledExecutorService.scheduleAtFixedRate(new Task_MatchGroup(),1, 60, TimeUnit.SECONDS);

        LocalDateTime midnight = LocalDateTime.now().plusDays(1).withHour(0).withMinute(0).withSecond(0).withNano(0);
        long delay_seconds = ChronoUnit.SECONDS.between(LocalDateTime.now(), midnight);
        long period = 60*60*24; /* 每天00：00刷新数据*/
        scheduledExecutorService.scheduleAtFixedRate(new Task_RefreshData(),delay_seconds, period,TimeUnit.SECONDS);
        scheduledExecutorService.scheduleAtFixedRate(new Task_SendTemplateMsg(),1, 60, TimeUnit.SECONDS);
    }
}
