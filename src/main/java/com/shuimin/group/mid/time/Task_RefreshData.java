package com.shuimin.group.mid.time;

import com.shuimin.group.constant.Config;
import com.shuimin.group.mid.redis.RedisKey;
import com.shuimin.group.mid.redis.RedisUtil;
import com.shuimin.group.util.Tool;
import pond.common.S;

import java.util.List;
import java.util.TimerTask;

public class Task_RefreshData extends TimerTask{

    @Override
    public void run() {
        List<String> list = RedisUtil.scanKey("*" + RedisKey.USER);
        S._for(list).each(item -> {

            if ("1".equals(RedisUtil.hGetFieldVal(item, "user_type"))) {
                RedisUtil.hSetFieldVal(item, RedisKey.USER_DAY_CREATE_TIMES, String.valueOf(Config.SHOP_DAY_CREATE_TIMES));
            } else {
                RedisUtil.hSetFieldVal(item, RedisKey.USER_DAY_CREATE_TIMES, String.valueOf(Config.USER_DAY_CREATE_TIMES));
            }
            RedisUtil.hSetFieldVal(item, RedisKey.USER_DAY_JOIN_TIMES, String.valueOf(Config.USER_DAY_JOIN_TIMES));
            RedisUtil.hSetFieldVal(item, RedisKey.USER_DAY_CANCEL_TIMES, String.valueOf(Config.USER_DAY_CANCEL_TIMES));
            RedisUtil.hSetFieldVal(item, RedisKey.USER_DAY_LEAVE_TIMES, String.valueOf(Config.USER_DAY_LEAVE_TIMES));
        });
    }
}
