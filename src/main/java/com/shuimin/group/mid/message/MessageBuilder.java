package com.shuimin.group.mid.message;

import pond.common.S;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessageBuilder {

    public static final String MSG_TYPE_TEXT = "text";
    public static final String MSG_TYPE_NEWS = "news";
    public static final String MSG_TYPE_EVENT = "event";
    public static final String MSG_TYPE_LINK = "link";
    public static final String MSG_TYPE_EVENT_SUBSCRIBE = "subscribe";
    public static final String MSG_TYPE_EVENT_UNSUBSCRIBE = "unsubscribe";

    public static final String HOST = "http://app.zuuzuu.cn";

    /**
     * 创建一条图文 ,并附上该列表url 并转化为xml形式
     *
     * @return
     */
    public static Map<String, Object> buildMultiMessage(String name, String url, Map<String, String> receivedMap) {
        Map<String, Object> map = new HashMap<>();
        map.put("ToUserName", receivedMap.get("FromUserName"));
        map.put("FromUserName", receivedMap.get("ToUserName"));
        map.put("CreateTime", S.now() / 1000);
        map.put("MsgType", MessageBuilder.MSG_TYPE_NEWS);
        map.put("ArticleCount", 1);
        List<Map> list = new ArrayList<>();
        Map<String, Object> _item = new HashMap<>();
        _item.put("Title", name);
        _item.put("Description", name);
        _item.put("PicUrl", HOST + "/images/logo.png");
        _item.put("Url", url + "openid=" + receivedMap.get("FromUserName"));
        list.add(_item);
        map.put("Articles", list);
        return map;
    }

    /**
     * 创建一条消息,仅包含普通文字
     *
     * @param text
     * @param receivedMap
     * @return
     */
    public static Map<String, Object> buildTextMessage(String text, Map<String, String> receivedMap) {
        Map<String, Object> map = new HashMap<>();
        map.put("ToUserName", receivedMap.get("FromUserName"));
        map.put("FromUserName", receivedMap.get("ToUserName"));
        map.put("CreateTime", S.now() / 1000);
        map.put("MsgType", MSG_TYPE_TEXT);
        map.put("Content", text);
        return map;
    }

}