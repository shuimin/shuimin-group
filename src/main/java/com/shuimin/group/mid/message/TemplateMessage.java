package com.shuimin.group.mid.message;

import com.shuimin.group.util.Tool;
import pond.common.S;
import pond.common.STRING;
import pond.common.f.Callback;

import java.util.HashMap;
import java.util.Map;

public enum TemplateMessage {
    ACTIVITY_OFF("9LcDx-mbr7dOrTPdMmSJobqNBIdPywv4WH_08B2aSso",
            "活动退出通知",
            (result, group, member) ->{

        /*
        {{first.DATA}}
        退出人：{{keyword1.DATA}}
        退出时间：{{keyword2.DATA}}
        {{remark.DATA}}
        *
        * */
                result.put("first", new HashMap(){{
                    put("value", group.get("group_name"));
                    put("color", "#173177");
                }});
                result.put("keyword1", new HashMap(){{
                    put("value", member.get("app_name"));
                    put("color", "#173177");
                }});
                result.put("keyword2", new HashMap(){{
                    put("value", member.get("leave_time"));
                    put("color", "#173177");
                }});
//                result.put("keyword3", new HashMap(){{
//                    put("value", "网吧");
//                    put("color", "#173177");
//                }});
//                result.put("keyword4", new HashMap(){{
//                    put("value", "wonder");
//                    put("color", "#173177");
//                }});
//                result.put("keyword5", new HashMap(){{
//                    put("value", "15151");
//                    put("color", "#173177");
//                }});
                if (STRING.notBlank())
                {
                    result.put("remark", new HashMap(){{
                        put("value", member.get("remark"));
                        put("color", "#173177");
                    }});
                }

            }),
    ACTIVITY_CANCEL("DcXvrWwdQ00AI2h_9jT89Q4oqKD7FO3jZvIvSmbn_do",
            "活动取消通知",
            (result, group,member) ->{
/*
* {{first.DATA}}
活动名称：{{keyword1.DATA}}
活动时间：{{keyword2.DATA}}
活动地点：{{keyword3.DATA}}
{{remark.DATA}}
* */
//                result.put("first", new HashMap(){{
//                    put("value", group.get("group_name"));
//                    put("color", "#173177");
//                }});
                result.put("keyword1", new HashMap(){{
                    put("value", member.get("group_name"));
                    put("color", "#173177");
                }});
                result.put("keyword2", new HashMap(){{
                    put("value", Tool.getDate(new Long(group.get("book_time")), "yyyy-MM-dd HH:mm:ss"));
                    put("color", "#173177");
                }});
                result.put("keyword3", new HashMap(){{
                    put("value", group.get("place_name"));
                    put("color", "#173177");
                }});
//                result.put("keyword4", new HashMap(){{
//                    put("value", "wonder");
//                    put("color", "#173177");
//                }});
//                result.put("keyword5", new HashMap(){{
//                    put("value", "15151");
//                    put("color", "#173177");
//                }});
                if (STRING.notBlank())
                {
                    result.put("remark", new HashMap(){{
                        put("value", group.get("remark"));
                        put("color", "#173177");
                    }});
                }
            }),
    ACTIVITY_CHANGE("OX4yguckJILXmEHAJCN4hFZY7TulnG-AdLv5RVRnRik","活动变动通知",
            (result, group,member) ->{
/*
{{first.DATA}}
活动名称：{{keyword1.DATA}}
活动时间：{{keyword2.DATA}}
活动地点：{{keyword3.DATA}}
{{remark.DATA}}
* */
//                result.put("first", new HashMap(){{
//                    put("value", group.get("group_name"));
//                    put("color", "#173177");
//                }});
                result.put("keyword1", new HashMap(){{
                    put("value", member.get("group_name"));
                    put("color", "#173177");
                }});
                result.put("keyword2", new HashMap(){{
                    put("value", Tool.getDate(new Long(group.get("book_time")), "yyyy-MM-dd HH:mm:ss"));
                    put("color", "#173177");
                }});
                result.put("keyword3", new HashMap(){{
                    put("value", group.get("place_name"));
                    put("color", "#173177");
                }});
//                result.put("keyword4", new HashMap(){{
//                    put("value", "wonder");
//                    put("color", "#173177");
//                }});
//                result.put("keyword5", new HashMap(){{
//                    put("value", "15151");
//                    put("color", "#173177");
//                }});
                if (STRING.notBlank())
                {
                    result.put("remark", new HashMap(){{
                        put("value", group.get("remark"));
                        put("color", "#173177");
                    }});
                }
            }),
    ACTIVITY_JOIN("bYb8Ob-KVOZj-8CpyeERM4FjXi4vtuoYXQnIfsiWM_w","活动报名成功提醒",
            (result, group,member) ->{
 /*
 * {{first.DATA}}
活动主题：{{keyword1.DATA}}
活动时间：{{keyword2.DATA}}
活动地点：{{keyword3.DATA}}
联系人：{{keyword4.DATA}}
联系方式：{{keyword5.DATA}}
{{remark.DATA}}
 * */

 /*
{{first.DATA}}
活动名称：{{keyword1.DATA}}
活动时间：{{keyword2.DATA}}
活动地点：{{keyword3.DATA}}
{{remark.DATA}}
* */
//                result.put("first", new HashMap(){{
//                    put("value", group.get("group_name"));
//                    put("color", "#173177");
//                }});
                result.put("keyword1", new HashMap(){{
                    put("value", group.get("group_name"));
                    put("color", "#173177");
                }});
                result.put("keyword2", new HashMap(){{
                    put("value", Tool.getDate(new Long(group.get("book_time")), "yyyy-MM-dd HH:mm:ss"));
                    put("color", "#173177");
                }});
                result.put("keyword3", new HashMap(){{
                    put("value", group.get("place_name"));
                    put("color", "#173177");
                }});
//                result.put("keyword4", new HashMap(){{
//                    put("value", "wonder");
//                    put("color", "#173177");
//                }});
//                result.put("keyword5", new HashMap(){{
//                    put("value", "15151");
//                    put("color", "#173177");
//                }});
                if (STRING.notBlank())
                {
                    result.put("remark", new HashMap(){{
                        put("value", group.get("remark"));
                        put("color", "#173177");
                    }});
                }
            }),
    ACTIVITY_SIGN("h4kDxfGF9spxv2KC6worG5a6JvJhdz8ZT1I3jTincXM","活动签到成功提醒",
            (result, group,member) ->{
/*
* {{first.DATA}}
客户姓名：{{keyword1.DATA}}
活动名称：{{keyword2.DATA}}
签到时间：{{keyword3.DATA}}
签到地点：{{keyword4.DATA}}
{{remark.DATA}}
* */
//                result.put("first", new HashMap(){{
//                    put("value", group.get("group_name"));
//                    put("color", "#173177");
//                }});
                result.put("keyword1", new HashMap(){{
                    put("value", member.get("app_name"));
                    put("color", "#173177");
                }});
                result.put("keyword2", new HashMap(){{
                    put("value", group.get("group_name"));
                    put("color", "#173177");
                }});
                result.put("keyword3", new HashMap(){{
                    put("value", Tool.getDate(new Long(group.get("sign_time")), "yyyy-MM-dd HH:mm:ss"));
                    put("color", "#173177");
                }});
                result.put("keyword4", new HashMap(){{
                    put("value", group.get("place_name"));
                    put("color", "#173177");
                }});
//                result.put("keyword5", new HashMap(){{
//                    put("value", "15151");
//                    put("color", "#173177");
//                }});
                if (STRING.notBlank())
                {
                    result.put("remark", new HashMap(){{
                        put("value", group.get("remark"));
                        put("color", "#173177");
                    }});
                }
            }),
    ACTIVITY_END("qgSnpdZc0bBzwUWxViqI50XlH748R-xZgE61mf8ebeU","活动结果通知",
            (result, group,member) ->{

            }),
    ACTIVITY_FEEDBACK("","用户反馈",
            (result, group, member) ->{

            });
    private String id ;
    private String title ;
    private Callback.C3<Map<String, Map<String,String>>, Map<String,String>,Map<String,String>> hander;
    private TemplateMessage(String id, String title,Callback.C3<Map<String,Map<String,String>>, Map<String,String>,Map<String,String>> hander){
        this.id = id ;
        this.title = title ;
        this.hander = hander;
    }
    public String id() {
        return this.id;
    }
    public String title() {
        return this.title;
    }
    public static TemplateMessage get(String id){
        for( TemplateMessage templateMessage : TemplateMessage.values()){
            if (templateMessage.id().equals(id) )
                return templateMessage;
        }
        return null;
    }
    public Map<String,Map<String,String>> message(Map<String,String> group, Map<String,String> member){
        Map<String,Map<String,String>> result =  new HashMap<>();
        this.hander.apply(result,group,member);
        S.echo(result);
        return result;
    }
}
