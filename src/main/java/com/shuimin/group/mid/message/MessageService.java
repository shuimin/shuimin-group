package com.shuimin.group.mid.message;

import com.shuimin.group.mid.redis.RedisKey;
import com.shuimin.group.mid.redis.RedisService;
import com.shuimin.group.mid.redis.RedisUtil;
import com.shuimin.group.model.group.VGroup;
import com.shuimin.group.service.WeixinService;
import pond.common.S;
import pond.common.STRING;

import java.util.*;

public class MessageService {

    public static void sendTemplateMsgToMulti(Map<String,String> data)
    {
        S._assertNotNull(data);
        String type = data.get("type");
        String group_id = data.get("group_id");
        String time =  data.get("time");
        S.echo("MessageService : sendTemplateMsg"+ group_id+":"+type+":"+new Date(new Long(time)));
        String groupKey = RedisService.getGroupKeyById(group_id);
        if (STRING.isBlank(groupKey))
        {
            return;
        }
        Map<String,String> groupMap = RedisUtil.hscanFromKey(groupKey);
        List<String> memberKeyList = RedisUtil.keys(group_id+"*"+ RedisKey.MEMBER);
        List<Map<String, String>> memberList = RedisUtil.hscanFromKeys(memberKeyList);

        TemplateMessage templateMessage = TemplateMessage.get(type);

        S._for(memberList).each(member->{
            Map<String,Map<String,String>> miniPro = new HashMap<>();
            Map<String,Map<String,String>> msgData = templateMessage.message(groupMap,  member);
            if (msgData.size() ==0)
            {
                return;
            }
            int errcode = WeixinService.sendTemplateMsg(member.get("open_id"), templateMessage.id(),"", miniPro, msgData);
            if (0 == errcode)
            {
            }
            else
            {
                S.echo(templateMessage.title()+ ":"+member.get("app_name")+":errcode="+ errcode);
            }
        });

    }

    public static void sendTemplateMsgToSingle(Map<String,String> data)
    {
        S._assertNotNull(data);
        String type = data.get("type");
        String group_id = data.get("group_id");
        String time =  data.get("time");
        String open_id =  data.get("open_id"); // 指接受人
        String app_name = data.get("app_name");
        S.echo("MessageService : sendTemplateMsg"+ group_id+":"+type+":"+new Date(new Long(time)));
        String groupKey = RedisService.getGroupKeyById(group_id);
        if (STRING.isBlank(groupKey))
        {
            return;
        }
        Map<String,String> groupMap = RedisUtil.hscanFromKey(groupKey);
        TemplateMessage templateMessage = TemplateMessage.get(type);
        Map<String,String> member = new HashMap<>();
        member.put("app_name", app_name);

        Map<String,Map<String,String>> msgData = templateMessage.message(groupMap, member );
        int errcode = WeixinService.sendTemplateMsg(open_id, templateMessage.id(),"", new HashMap<>(), msgData);
        if (0 == errcode)
        {

        }
        else
        {
            S.echo(templateMessage.title()+ ":"+app_name+":errcode="+ errcode);
        }

    }
}
