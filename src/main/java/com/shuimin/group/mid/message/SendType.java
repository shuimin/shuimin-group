package com.shuimin.group.mid.message;

public enum SendType {
    SINGLE("0"),
    MULTIPLE("1");
    private String value ;
    private SendType(String value){
        this.value = value ;
    }
    public String val() {
        return this.value;
    }
    public static SendType get(String val){
        for( SendType sendType : SendType.values()){
            if (sendType.val().equals(val))
                return sendType;
        }
        return null;
    }
}
