package com.shuimin.group.mid.redis;

public class RedisKey {

    public static final String GROUP_HEAD = "head";//局
    public static final String GROUP = ":group";//局
    public static final String MEMBER = ":member";//参与者
    public static final String SIGN = ":sign";//签到

    public static final String PLACE = ":place";//场地
    public static final String USER = ":user"; //用户

    public static final String TO_DO_TEMPLATE_MSG = "todo_template_msg"; //等待发送消息
    public static final String TEMPLATE_MSG = ":template_msg"; //模版消息

    public static final String CHAT_RECORD = ":chat_record"; //聊天记录
    public static final String CHAT_MSG_UNREAD= ":chat_msg_unread"; //聊天未读记录
    public static final String CHAT_MSG = ":chat_msg";//聊天信息

    public static final String WEIXIN_USER_UID = "weixin_user_uid";//微信用户ID
    public static final String GROUP_LIFE_CYCLE = "group_life_cycle";//局的生命周期
//
//    public static final String GROUP_JOIN_END_TIME = "group_join_end_time";//
//    public static final String GROUP_NOTIFY_DELAY_TIME = "group_notify_delay_time";//
//    public static final String GROUP_CREATE_MIN_TIME = "group_create_min_time";//
//    public static final String GROUP_CREATE_MAX_TIME = "group_create_min_time";//
//    public static final String GROUP_SEND_SIGN_TIME = "group_send_sign_time";//
//    public static final String GROUP_CALC_SIGN_TIME = "group_calc_sign_time";
//    public static final String GROUP_SEND_FEEDBACK_TIME = "group_send_feedback_time";//

    public static final String USER_DAY_JOIN_TIMES = "user_day_join_times";//
    public static final String USER_DAY_CREATE_TIMES = "user_day_create_times";//
    public static final String USER_DAY_LEAVE_TIMES = "user_day_leave_times";//
    public static final String USER_DAY_CANCEL_TIMES = "user_day_cancel_times";//
}
