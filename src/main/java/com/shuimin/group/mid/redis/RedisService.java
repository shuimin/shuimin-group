package com.shuimin.group.mid.redis;

import com.shuimin.group.constant.GroupStatus;
import com.shuimin.group.model.group.VGroup;
import com.shuimin.group.model.group.VGroupMember;
import pond.common.S;
import pond.common.STRING;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RedisService {

    public static List<String> getAllKeysAboutGroup(String groupId,String groupKey) {
        //todo 删除局信息，成员信息，签到信息
        List<String> list = new ArrayList<>();
        list.add(groupKey);
        list.addAll(RedisUtil.scanKey(groupId + "*" + RedisKey.MEMBER));
        list.add(RedisService.getKey(groupId, RedisKey.SIGN));

        //todo 聊天室信息
        //delKeyList.addAll(RedisUtil.scanKey(group_id+"*"+ RedisKey.CHAT_RECORD));
        //delKeyList.addAll(RedisUtil.scanKey(group_id+"*"+ RedisKey.CHAT_MSG_UNREAD));
        //todo 从 CHAT_RECORD 和 CHAT_MSG_UNREAD 获取 MSG_ID 删除
        return list;
    }
    public static String getGroupKey(VGroup item) {
        //自动次序关联 RedisGroupKey
        // create_type:place_id:time:delay:group_id (group_type 放在二级查询)
        String key = "";
        if (GroupStatus.GROUP_STATUS_CREATE.val().equals(item.get("group_status").toString())) {
            key =   RedisKey.GROUP_HEAD
                    + ":" + item.get("create_type")
                    + ":" + item.get("place_id")
                    + ":" + item.get("book_time")
                    + ":" + item.get("max_num")
                    + ":" + item.id()
                    + RedisKey.GROUP;
        }
        return key;
    }

    public static String getGroupKeyById(String id){
        S._assertNotNull(id);
        List<String> keyList = new ArrayList<>();
        if (STRING.notBlank(id))
        {
            String pattern = "*"+id+RedisKey.GROUP;
            keyList = RedisUtil.scanKey(pattern);
        }
        if ( null != keyList && keyList.size() ==1)
        {
            return keyList.get(0);
        }
        return "";
    }
    public static List<String> getGroupKeys()
    {
        List<String> groupkeyList = new ArrayList<>();
        groupkeyList.addAll(RedisUtil.scanKey(RedisKey.GROUP_HEAD+ "*"+ RedisKey.GROUP));
        return  groupkeyList;
    }
    public static List<String> getKeysFromParams(Map<String,Object> params)
    {
//        head(0),
//                create_type(1),
//                place_id(2),
//                book_time(3),
//                max_num(4),
//                group_id(5),
//                group_key(6);
        List<String> retList = new ArrayList<>();
        String[] group_status;
        String placeId = "";
        if (params.containsKey("place_id"))
        {
            placeId = params.get("place_id").toString();
        }
        String id = "";
        if (params.containsKey("id"))
        {
            id = params.get("id").toString();
        }

        String pattern = RedisKey.GROUP_HEAD+"*";
        if (STRING.notBlank(placeId))
        {
            pattern = pattern+":"+placeId;
        }
        pattern = pattern+"*";
        if (STRING.notBlank(id))
        {
            pattern = pattern+id;
        }
        pattern = pattern+RedisKey.GROUP;
        S.echo(pattern);
        //todo 时间判断
        retList.addAll(RedisUtil.scanKey(pattern));
        S.echo("getKeysFromParams :"+retList);
        return retList;
    }
    public static String getKey(String groupId, String key) {
        S._assertNotNull(groupId);
        return new String(groupId + key);
    }
    public static String getKey(String groupId, String userId, String key) {
        S._assertNotNull(groupId,userId);
        return new String(groupId + ":" + userId + key);
    }
    public static String getMemberKey(String groupId, String userId) {
        S._assertNotNull(groupId,userId);
        return new String(groupId + ":" + userId + RedisKey.MEMBER);
    }
    public static Map<String,String> getMemberValue(VGroupMember member)
    {
        S._assertNotNull(member);
        return new HashMap(){{
            if (null != member.get("user_id"))
                put("user_id",  member.get("user_id"));
            if (null != member.get("op_time"))
                put("op_time",  member.get("op_time"));
            if (null != member.get("is_creator"))
                put("is_creator",  member.get("is_creator"));
            if (null != member.get("app_name"))
                put("app_name",  member.get("app_name"));
            if (null != member.get("app_avatar_url"))
                put("app_avatar_url",  member.get("app_avatar_url"));
            if (null != member.get("uid"))
                put("uid",  member.get("uid"));
            if (null != member.get("user_type"))
                put("user_type",  member.get("user_type"));
        }};
    }
    public static Map<String,Object> getMemberVal(VGroupMember member)
    {
        S._assertNotNull(member);
        return new HashMap(){{
            if (null != member.get("user_id"))
                put("user_id",  member.get("user_id"));
            if (null != member.get("op_time"))
                put("op_time",  member.get("op_time"));
            if (null != member.get("is_creator"))
                put("is_creator",  member.get("is_creator"));
            if (null != member.get("app_name"))
                put("app_name",  member.get("app_name"));
            if (null != member.get("app_avatar_url"))
                put("app_avatar_url",  member.get("app_avatar_url"));
            if (null != member.get("uid"))
                put("uid",  member.get("uid"));
            if (null != member.get("user_type"))
                put("user_type",  member.get("user_type"));
        }};
    }
    public static boolean exist(String key) {
        return  RedisUtil.exist(key);
    }
    public static boolean existMsgKey(String msgId) {
        return  RedisUtil.exist(msgId + RedisKey.CHAT_MSG);
    }
    public static boolean existKey(String groupId, String userId, String redisKey) {
        return RedisUtil.exist(RedisService.getKey(groupId, userId, redisKey));
    }
    public synchronized static long getWeiXinUserUid() {
        return RedisUtil.incr(RedisKey.WEIXIN_USER_UID);
    }


}
