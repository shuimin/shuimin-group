package com.shuimin.group.mid.redis;

public enum RedisGroupKey {
    head(0),
    create_type(1),
    place_id(2),
    book_time(3),
    max_num(4),
    group_id(5),
    group_key(6);
    private int value ;
    private RedisGroupKey(int value){
        this.value = value ;
    }
    public int val() {
        return this.value;
    }
    public static RedisGroupKey get(int val){
        for( RedisGroupKey groupKey : RedisGroupKey.values()){
            if (groupKey.val()== val)
                return groupKey;
        }
        return null;
    }
}