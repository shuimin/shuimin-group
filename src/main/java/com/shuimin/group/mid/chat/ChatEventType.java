package com.shuimin.group.mid.chat;

public enum ChatEventType {
    MESSAGE("message");
    private String type ;
    private ChatEventType(String type){
        this.type = type ;
    }
    public String val() {
        return this.type;
    }
}
