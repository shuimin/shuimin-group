package com.shuimin.group.mid.chat;
import com.shuimin.group.model.weixin.WeixinUser;
import pond.common.JSON;
import pond.common.S;
import pond.common.STRING;

import java.util.HashMap;
import java.util.Map;

public class ChatEvent {
    private final String id;
    private String type;
    private String timestamp;
    private String content;

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    private String group_id;
    private String user_id;
    private String nick_name;
    private String avatarUrl;

    private ChatEvent(Map<String, Object> map){
        id = S.uuid.vid();
        if (map.containsKey("type"))
            this.type = map.get("type").toString();
        if (map.containsKey("timestamp"))
            this.timestamp = map.get("timestamp").toString();
        if (map.containsKey("content"))
            this.content =  map.get("content").toString();
        if (map.containsKey("group_id"))
            this.group_id =  map.get("group_id").toString();
        if (map.containsKey("user_id"))
            this.user_id = map.get("user_id").toString();
        if (map.containsKey("nick_name"))
            this.nick_name =  map.get("nick_name").toString();
        if (map.containsKey("avatarUrl"))
            this.avatarUrl =  map.get("avatarUrl").toString();
    }
    public static ChatEvent message(String msg, WeixinUser weixinUser){

        Map <String, Object> map  = JSON.parse(msg);
        if (null == map || null == map.get("type") || null == map.get("content") || null == map.get("group_id"))
        {
            S.echo("not valid " + msg);
            return null;
        }
        if (null == weixinUser || null == weixinUser.get("id") || null == weixinUser.get("nick_name") || null == weixinUser.get("avatarUrl"))
        {
            S.echo("not valid " + weixinUser);
            return null;
        }
        map.put("user_id", weixinUser.get("id"));
        map.put("nick_name", weixinUser.get("nick_name"));
        map.put("avatarUrl", weixinUser.get("avatarUrl"));
        if (null == map.get("timestamp"))
        {
            map.put("timestamp", S.now());
        }

        return new ChatEvent(map);
    }
    public static ChatEvent keepAlive(){
        return new ChatEvent(new HashMap<String, Object>(){{
            put("type", "keep-alive");
            put("timestamp", S.now());
            put("content", "");
        }});
    }

    public Map<String, String> toJedisMap(){
        return new HashMap<String, String>(){{
            if (STRING.notBlank(id))
                put("id", id);
            if (STRING.notBlank(type))
                put("type", type);
            if (STRING.notBlank(timestamp))
                put("timestamp", timestamp);
            if (STRING.notBlank(content))
                put("content", content);
            if (STRING.notBlank(group_id))
                put("group_id", group_id);
            if (STRING.notBlank(user_id))
                put("user_id", user_id);
            if (STRING.notBlank(nick_name))
                put("nick_name", nick_name);
            if (STRING.notBlank(avatarUrl))
                put("avatarUrl", avatarUrl);
        }};
    }
    public String toJSON(){
        return JSON.stringify(S._tap(new HashMap<String, Object>(), m -> {
            if (STRING.notBlank(id))
                m.put("id", id);
            if (STRING.notBlank(type))
                m.put("type", type);
            if (STRING.notBlank(timestamp))
                m.put("timestamp", timestamp);
            if (STRING.notBlank(content))
                m.put("content", content);
            if (STRING.notBlank(group_id))
                m.put("group_id", group_id);
            if (STRING.notBlank(user_id))
                m.put("user_id", user_id);
            if (STRING.notBlank(nick_name))
                m.put("nick_name", nick_name);
            if (STRING.notBlank(avatarUrl))
                m.put("avatarUrl", avatarUrl);
        }));
    }
}
