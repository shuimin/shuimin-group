package com.shuimin.group;

import com.shuimin.group.service.WeixinUserService;
import pond.common.S;
import pond.common.STRING;
import pond.web.Render;
import pond.web.Router;

import java.util.List;
import java.util.Map;

public class INNER_WEIXIN_USER extends Router {
    {
        get("/", (req, resp) -> {
            String id = req.param("id");
            S.echo("id:"+id);
            if (STRING.isBlank(id)) {
                List<Map<String, Object>> ret = WeixinUserService.all();
                resp.render(Render.json(ret));
            }
            else {
                resp.render(Render.json(WeixinUserService.getWeixinById(id)));
            }
        });
    }

}
