package com.shuimin.group;

import com.shuimin.group.model.weixin.WeixinUser;
import com.shuimin.group.model.place.Place;
import com.shuimin.group.model.place.PlaceType;
import com.shuimin.group.service.PlaceService;
import pond.common.S;
import pond.common.STRING;
import pond.common.f.Tuple;
import pond.web.Render;
import pond.web.Router;
import pond.web.Session;

import java.util.*;

public class PLACE extends Router {
    {
        /* 查询场地 */
        get("/", (req, resp) -> {
            Map<String,Object> params = new HashMap<>();
            String  gps_x  = req.param("gps_x");
            String  gps_y  = req.param("gps_y");
            String  distance = req.param("distance");
            if (STRING.notBlank(gps_x) && STRING.notBlank(gps_y) && STRING.notBlank(distance))
            {
                params.put(PlaceService.PLACE_QUERY_KEY_GPS_DISTANSE, Tuple.t3(Double.valueOf(gps_x),Double.valueOf(gps_y),Double.valueOf(distance)));
            }
            List<Map<String,Object>> ret = PlaceService.listFromParams(params);
            if (params.containsKey(PlaceService.PLACE_QUERY_KEY_GPS_DISTANSE))
            {
                Collections.sort(ret,  new Comparator<Map<String, Object>>() {
                    @Override
                    public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                        double a = (double)(o1.get("distance"));
                        double b = (double)(o2.get("distance"));
                        //升序
                        return a > b ? 1 : -1 ;
                    }
                });
            }
            resp.render(Render.page(ret, ret.size()));
        });

        get("/place_type", (req, resp) -> {
            resp.render(Render.json(PlaceService.allPlaceType()));
        });

        /* 查询某个场地具体信息 */
        get("/:id", (req, resp) -> {
            String id = req.paramNonBlank("id", "id 不能为空");
            resp.render(Render.json(App.DB.get(t -> t.recordById(Place.class, id))));
        });

        /* 新建场地 */
        post("/", (req, resp) -> {
            Place place =  new Place();
            place.merge(req.toMap());
            place.setId(S.uuid.vid());
            if (place.get("create_time") == null)
            {
                place.set("create_time", new Date().getTime());;
            }
            place.set("is_delete","n");
            Session ses = Session.get(req);
            WeixinUser weixinUser = ses.get(App.APP_WEIXIN_USER_INFO);
            if (weixinUser !=null)
            {
                place.set("creator_id", weixinUser.id());
                place.set("creator_name", weixinUser.get("nick_name"));
            }

            App.DB.post(t -> {t.recordInsert(place); });
            resp.send(200);
        });

        post("/place_type/", (req, resp) -> {
            String openId = req.paramNonBlank("id", "id 不能为空");
            PlaceType placeType =  new PlaceType();
            placeType.merge(req.toMap());
            placeType.setId(S.uuid.vid());
            App.DB.post(t -> {t.recordInsert(placeType); });
            resp.send(200);
        });

        /* 更新场地 */
        put("/:id", (req, resp) -> {
            App.DB.post(t -> {
                String id = req.paramNonBlank("id", "id 不能为空");
                Place place = PlaceService.get(t, id);
                if (place ==null)
                {
                    resp.send(404,id+ "not found");
                    return;
                }
                else {
                    place.merge(req.toMap());
                    place.setId(id);
                    place.set("is_delete", "n");
                    t.recordUpdate(place);
                }
            });
        });
        /* 删除场地 */
        del("/:id", (req, resp) -> {
            App.DB.post(t -> {
                String id = req.paramNonBlank("id", "id 不能为空");
                Place place = PlaceService.get(t, id);
                if (place !=null )
                {
                    place.set("is_delete", "y");
                    t.recordUpdate(place);
                }
                resp.send(200);
            });
        });
        del("/place_type/:id", (req, resp) -> {
            String id = req.paramNonBlank("id", "id 不能为空");
            App.DB.post(t -> {t.recordDelete(new PlaceType().setId(id)); });
            resp.send(200);
        });
    }
}