package com.shuimin.group.service;

import com.shuimin.group.App;
import com.shuimin.group.model.follow.FollowGroupType;
import com.shuimin.group.model.follow.FollowPlace;
import com.shuimin.group.model.follow.FollowUser;
import pond.common.S;
import pond.db.sql.Criterion;
import pond.db.sql.Sql;
import pond.db.sql.SqlSelect;

import java.util.List;

public class FollowService {
    public static SqlSelect allFollowGroupType(String userId) {
        S._assertNotNull(userId);
        return Sql.select("*").from("t_follow_group_type")
                .where("is_delete",Criterion.EQ,"n")
                .where("user_id", Criterion.EQ, userId);
    }
    public static FollowGroupType getFollowGroupType(String userId, String groupTypeId)
    {
        S._assertNotNull(userId,groupTypeId);
        List<FollowGroupType> list = App.DB.get(t -> {
                return t.query(FollowGroupType.class,
                "select * from t_follow_group_type " +
                        " where is_delete = 'n'" +
                        " and user_id = '" + userId +"' "+
                        " and group_type_id =  '"+ groupTypeId + "';");
            });
        if (list != null && list.size() >0)
        {
            return list.get(0);
        }
        return null;
    }

    public static SqlSelect allUser(String userId) {
        S._assertNotNull(userId);
        return Sql.select("*").from("t_weixin_user")
                .join("t_follow_user")
                .on("t_follow_user.follow_user_id = t_weixin_user.id ")
                .where("t_follow_user.is_delete = 'n'")
                .where("t_follow_user.user_id ='" + userId+"'");

    }

    public static FollowUser getFollowUser(String userId, String followUserId)
    {
        S._assertNotNull(userId,followUserId);
        List<FollowUser> list = App.DB.get(t -> {
            return t.query(FollowUser.class,
                    "select * from t_follow_user " +
                            " where is_delete = 'n'" +
                            " and user_id = '" + userId +"'" +
                            " and follow_user_id = '" + followUserId + "';");
        });
        if (list != null && list.size() >0)
        {
            return list.get(0);
        }
        return null;
    }
    public static SqlSelect allPlace(String userId) {
        S._assertNotNull(userId);
        return Sql.select("*").from("t_place")
                .join("t_follow_place")
                .on("t_follow_place.place_id = t_place.id ")
                .where("t_follow_place.is_delete = 'n'")
                .where("t_follow_place.user_id = '"+ userId+"'");
    }
    public static FollowPlace getFollowPlace(String userId, String placeId)
    {
        S._assertNotNull(userId,placeId);
        List<FollowPlace> list = App.DB.get(t -> {
            return t.query(FollowPlace.class,
                    "select * from t_follow_place " +
                            " where is_delete = 'n'" +
                            " and user_id ='" + userId +"'"+
                            " and place_id ='" + placeId + "';");
        });
        if (list != null && list.size() >0)
        {
            return list.get(0);
        }
        return null;
    }
}
