package com.shuimin.group.service;


import com.shuimin.group.App;
import com.shuimin.group.logic.UserLogic;
import com.shuimin.group.mid.redis.RedisKey;
import com.shuimin.group.mid.redis.RedisService;
import com.shuimin.group.mid.redis.RedisUtil;
import com.shuimin.group.model.weixin.WeixinUser;
import pond.common.S;
import pond.common.STRING;
import pond.db.JDBCTmpl;
import pond.db.sql.Criterion;
import pond.db.sql.Sql;
import pond.db.sql.SqlSelect;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class WeixinUserService {
    public static void add(WeixinUser weixinUser) {
        S._assertNotNull(weixinUser, weixinUser.id());
        if (null == weixinUser.get("uid") || STRING.isBlank(weixinUser.get("uid").toString()))
        {
            weixinUser.set("uid", RedisService.getWeiXinUserUid());
        }
        App.DB.post(t->t.recordInsert(weixinUser));
    }
    public static void update(WeixinUser weixinUser) {
        S._assertNotNull(weixinUser, weixinUser.id());
        App.DB.post(t->t.recordUpdate(weixinUser));
    }
    public static WeixinUser get(String openid) {
        S._assertNotNull(openid);
        SqlSelect sql = Sql.select("*").from("t_weixin_user").where("open_id", Criterion.EQ,openid);
        List<WeixinUser> list = App.DB.get(t->t.query(WeixinUser.class,sql));
        if (list !=null && list.size() >0)
        {
            WeixinUser weixinUser = list.get(0);
            if (null == weixinUser.get("uid") || STRING.isBlank(weixinUser.get("uid").toString()))
            {
                weixinUser.set("uid", RedisService.getWeiXinUserUid());
                App.DB.post(t->t.recordUpdate(weixinUser));
            }
            return weixinUser;
        }
        return null;
    }
    public static WeixinUser mergeObj(String openid, Map<String,Object> userInfo) {
        WeixinUser newWeixinUser = WeixinUserService.get(openid);
        if (newWeixinUser == null)
        {
            newWeixinUser = new WeixinUser();
            newWeixinUser.setId(S.uuid.vid());
            newWeixinUser.set("user_type", "0");
            newWeixinUser.set("uid", RedisService.getWeiXinUserUid());
            newWeixinUser.set("open_id", openid);
            if (userInfo.containsKey("nickname"))
            {
                newWeixinUser.set("nick_name", userInfo.get("nickname"));
                newWeixinUser.set("app_name", userInfo.get("nickname"));
            }
            if (userInfo.containsKey("nickName"))
            {
                newWeixinUser.set("nick_name", userInfo.get("nickName"));
                newWeixinUser.set("app_name", userInfo.get("nickName"));
            }

            if (userInfo.containsKey("gender"))
            {
                newWeixinUser.set("gender", userInfo.get("gender"));
            }
            if (userInfo.containsKey("city"))
            {
                newWeixinUser.set("city", userInfo.get("city"));
            }
            if (userInfo.containsKey("province"))
            {
                newWeixinUser.set("province", userInfo.get("province"));
            }
            if (userInfo.containsKey("country"))
            {
                newWeixinUser.set("country", userInfo.get("country"));
            }
            if (userInfo.containsKey("avatarUrl"))
            {
                newWeixinUser.set("avatarUrl", userInfo.get("avatarUrl"));
                newWeixinUser.set("app_avatar_url", userInfo.get("avatarUrl"));
            }
            if (userInfo.containsKey("headimgurl"))
            {
                newWeixinUser.set("avatarUrl", userInfo.get("headimgurl"));
                newWeixinUser.set("app_avatar_url", userInfo.get("headimgurl"));
            }
            if (userInfo.containsKey("subscribe"))
            {
                newWeixinUser.set("subscribe", userInfo.get("subscribe"));
            }
            if (userInfo.containsKey("subscribe_time"))
            {
                newWeixinUser.set("subscribe_time", userInfo.get("subscribe_time"));
            }
            if (userInfo.containsKey("tagid_list"))
            {
                newWeixinUser.set("tagid_list", userInfo.get("tagid_list"));
            }
            if (userInfo.containsKey("groupid"))
            {
                newWeixinUser.set("groupid", userInfo.get("groupid"));
            }
            if (userInfo.containsKey("language"))
            {
                newWeixinUser.set("language", userInfo.get("language"));
            }
            if (userInfo.containsKey("remark"))
            {
                newWeixinUser.set("remark", userInfo.get("remark"));
            }
            if (userInfo.containsKey("sex"))
            {
                newWeixinUser.set("gender", userInfo.get("sex"));
            }
            WeixinUserService.add(newWeixinUser);
            RedisUtil.hAddOne(newWeixinUser.id()+RedisKey.USER, UserLogic.transfer(newWeixinUser));
        }
        else {
            boolean isUpdate =false;
            if (STRING.isBlank(newWeixinUser.get("uid")))
            {
                newWeixinUser.set("uid", RedisService.getWeiXinUserUid());
                isUpdate =true;
            }
            if (STRING.isBlank(newWeixinUser.get("user_type")))
            {
                newWeixinUser.set("user_type", "0");
                isUpdate =true;
            }
            if (userInfo.containsKey("avatarUrl") &&  !userInfo.get("avatarUrl").equals(newWeixinUser.get("avatarUrl")))
            {
                newWeixinUser.set("avatarUrl", userInfo.get("avatarUrl"));
                if (STRING.isBlank(newWeixinUser.get("app_avatar_url")))
                {
                    newWeixinUser.set("app_avatar_url", userInfo.get("avatarUrl"));
                }
                isUpdate = true;
            }
            if (userInfo.containsKey("nickName") &&  !userInfo.get("nickName").equals(newWeixinUser.get("nick_name")))
            {
                newWeixinUser.set("nick_name", userInfo.get("nickName"));
                if (STRING.isBlank(newWeixinUser.get("app_name")))
                {
                    newWeixinUser.set("app_name", userInfo.get("nickName"));
                }
                isUpdate = true;
            }
            if (userInfo.containsKey("nickname") &&  !userInfo.get("nickname").equals(newWeixinUser.get("nick_name")))
            {
                newWeixinUser.set("nick_name", userInfo.get("nickname"));
                if (STRING.isBlank(newWeixinUser.get("app_name")))
                {
                    newWeixinUser.set("app_name", userInfo.get("nickname"));
                }
                isUpdate = true;
            }
            if (userInfo.containsKey("headimgurl") &&  !userInfo.get("headimgurl").equals(newWeixinUser.get("avatarUrl")))
            {
                newWeixinUser.set("avatarUrl", userInfo.get("headimgurl"));
                newWeixinUser.set("app_avatar_url", userInfo.get("headimgurl"));
                isUpdate = true;
            }
            if (isUpdate) {
                WeixinUserService.update(newWeixinUser);
                RedisUtil.hAddOne(newWeixinUser.id()+RedisKey.USER, UserLogic.transfer(newWeixinUser));
            }
        }
        return newWeixinUser;

    }
    public static WeixinUser getUserById(String id) {
        WeixinUser weixinUser =  App.DB.get(t->{
              return t.recordById(WeixinUser.class, id);
        });
        return weixinUser;
    }
    public static void del( String id) {
        S._assertNotNull(id);
        App.DB.post(t->{
            if (t.recordExists(WeixinUser.class, id)) {
                t.recordDelete(new WeixinUser().setId(id));
            }
        });
    }
    public static boolean isExists(String id) {
        S._assertNotNull(id);
        return App.DB.get(t->t.recordExists(WeixinUser.class, id));
    }

    public static List all() {
        List<String> keyList = RedisUtil.scanKey("*"+ RedisKey.USER);
        return RedisUtil.hscanListFromKeys(keyList);
    }
    public static Map<String,Object> getWeixinById(String id) {
        S._assertNotNull(id);
        String key = id+ RedisKey.USER;
        return RedisUtil.hscanObjectsFromKey(key);
    }

}
