package com.shuimin.group.service;

import com.shuimin.group.App;
import com.shuimin.group.logic.LocationLogic;
import com.shuimin.group.model.group.VGroup;
import com.shuimin.group.model.place.Place;
import com.shuimin.group.model.place.PlaceType;
import pond.common.JSON;
import pond.common.S;
import pond.common.STRING;
import pond.common.f.Tuple;
import pond.db.JDBCTmpl;
import pond.db.sql.Criterion;
import pond.db.sql.Sql;
import pond.db.sql.SqlSelect;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PlaceService {
    public static final String PLACE_QUERY_KEY_GPS_DISTANSE = "gps";
    public static List<PlaceType> allPlaceType() {
        return App.DB.get(t -> t.query(PlaceType.class,"select * from t_place_type;"));
    }
    public static boolean isOk (Place place,Map<String,Object> params)
    {
        if (null == params)
        {
            return true;
        }
        final boolean[] ret = {true};

        S._for(params).each(
                param->{
                    if (param.getKey().equals(PLACE_QUERY_KEY_GPS_DISTANSE))
                    {
                        //todo 可以优化
                        Tuple.T3<Double,Double,Double> gps = (Tuple.T3<Double,Double,Double>) param.getValue();
                        double[] gps_ranges = LocationLogic.squarePoint(gps._a,gps._b,gps._c);
                        double gps_x = Double.valueOf(place.get("gps_x")).doubleValue();
                        double gps_y = Double.valueOf(place.get("gps_y")).doubleValue();
                        if (gps_x < gps_ranges[0] || gps_x > gps_ranges[1] || gps_y < gps_ranges[2] || gps_y > gps_ranges[3] )
                        {
                            ret[0] = false;
                            return;
                        }
                    }
                }
        );

        return ret[0];
    }
    public static List<Map<String,Object>> listFromParams(Map<String, Object> params) {
        SqlSelect select = Sql.selectFromRequest(params, Place.class)
                .where("is_delete", Criterion.EQ,"n");
        List<Place> list = App.DB.get(t->t.query(Place.class,select));
        List<Map<String,Object>> ret = new ArrayList<>();
        S._for(list).each(place ->{
            if (isOk(place,params))
            {
                Map <String,Object> map = mapObjFromPlace(place);
                if (params.containsKey(PLACE_QUERY_KEY_GPS_DISTANSE))
                {
                    Tuple.T3<Double,Double,Double> gps = (Tuple.T3<Double,Double,Double>) params.get(PLACE_QUERY_KEY_GPS_DISTANSE);
                    double gps_x = Double.valueOf(place.get("gps_x")).doubleValue();
                    double gps_y = Double.valueOf(place.get("gps_y")).doubleValue();
                    map.put("distance", LocationLogic.figuredMiles(gps_x,gps_y,gps._a,gps._b));
                }
                ret.add(map);
            }
        });
        return ret;
    }
    public static SqlSelect allPlace(Map<String, Object> params) {
        SqlSelect select = Sql.selectFromRequest(params, Place.class)
                .where("is_delete", Criterion.EQ,"n");
        return select;
    }
    public static SqlSelect allPlace() {
        SqlSelect select = Sql.select("*").from("t_place")
                .where("is_delete", Criterion.EQ,"n");
        return select;
    }
    public static Place get(JDBCTmpl t, String id) {
        S._assertNotNull(t,id);
        SqlSelect sql = Sql.select("*").from("t_place")
                .where("id", Criterion.EQ,id)
                .where("is_delete",Criterion.EQ,"n");
        List<Place> list = t.query(Place.class,sql);
        if (list !=null && list.size() >0)
        {
            return list.get(0);
        }
        return null;
    }
    public static boolean isExists(JDBCTmpl t, String id) {
        S._assertNotNull(t, id);
        return t.recordExists(Place.class, id);
    }

    public static Map<String,Object> mapObjFromPlace(Place place) {
        Map<String,Object> ret= new HashMap<>();
        ret.put("id", place.get("id"));
        ret.put("place_name", place.get("place_name"));
        ret.put("place_address", place.get("place_address"));
        ret.put("place_type_id", place.get("place_type_id"));
        ret.put("place_type", place.get("place_type"));
        ret.put("place_phone", place.get("place_phone"));
        ret.put("gps_x", place.get("gps_x"));
        ret.put("gps_y", place.get("gps_y"));
        ret.put("creator_id", place.get("creator_id"));
        ret.put("creator_name", place.get("creator_name"));
        ret.put("create_time", place.get("create_time"));
        ret.put("place_photo_url", place.get("place_photo_url"));
        ret.put("is_delete", place.get("is_delete"));
        return ret;
    }
    public static Map<String,Object> mapObjFromVGroup(VGroup vGroup) {
        Map<String,Object> place= new HashMap<>();
        place.put("id", vGroup.get("place_id"));
        place.put("place_name", vGroup.get("place_name"));
        place.put("place_address", vGroup.get("place_address"));
        place.put("place_type_id", vGroup.get("place_type_id"));
        place.put("place_type", vGroup.get("place_type"));
        place.put("place_phone", vGroup.get("place_phone"));
        place.put("gps_x", vGroup.get("gps_x"));
        place.put("gps_y", vGroup.get("gps_y"));
        place.put("creator_id", vGroup.get("place_creator_id"));
        place.put("creator_name", vGroup.get("place_creator_name"));
        place.put("create_time", vGroup.get("place_create_time"));
        place.put("place_photo_url", vGroup.get("place_photo_url"));
        place.put("is_delete", vGroup.get("is_delete"));
        return place;
    }
}
