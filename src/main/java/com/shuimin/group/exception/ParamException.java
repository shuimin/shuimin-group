package com.shuimin.group.exception;

public class ParamException extends UserCausedException {

    public int code = 400;

    public ParamException(String error) {
        super(400, error);
    }

    public ParamException(String param, String error) {
        super(400, param + "[" + error + "]");
    }

    public ParamException(int code, String error) {
        super(code, error);
        this.code = code;
    }

}