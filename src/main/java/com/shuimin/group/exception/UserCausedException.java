package com.shuimin.group.exception;

public class UserCausedException extends RuntimeException {

    int code;

    public UserCausedException(int code, String message) {
        super(message);
        this.code = code;
    }

    public UserCausedException(String message) {
        this(-1, message);
    }

    public UserCausedException() {
        this(-1, "unknown");
    }

}
